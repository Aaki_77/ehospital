//package com.hollongtree.ehospital.controller.hospitalController;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MockMvcBuilder;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//
//@RunWith(SpringJUnit4ClassRunner.class)
//public class PatientControllerTest {
//
//    private MockMvc mockMvc;
//
//    @InjectMocks
//    private PatientController patientController;
//
//    @Before
//    public void setUp() throws Exception {
//        mockMvc = MockMvcBuilders.standaloneSetup(patientController).build();
//    }
//
//    @Test
//    public void testPatientDetails() throws Exception {
//
//        mockMvc.perform(
//                MockMvcRequestBuilders.post("/savePatient")
//        )
//        .andExpect(MockMvcResultMatchers.status().isCreated())
//        .andExpect(MockMvcResultMatchers.content().json("\"patientid\":\"17\",\"patientname\":\"Rohita\",\"patientmobile\":\"9330255196\",\"govtHealthid\":\"Pancard\",\"age\":23,\"address1\":\"23\",\"address2\":\"23\",\"address3\":\"23\",\"address4\":\"23\",\"address5\":\"23\",\"dateofbirth\":\"07/12/1997\",\"gender\":\"M\",\"weight\":60,\"weighttype\":\"normal\",\"height\":6,\"heighttype\":\"Average\",\"udf1\":\"1\",\"udf2\":\"a\",\"udf3\":\"b\",\"udf4\":\"c\",\"udf5\":\"d\",\"bloodGroup\":\"A+\",\"prescriptions\":[],\"login\":[{\"loginhistoryid\":1,\"patient\":[],\"logintime\":\"01/01/2020\",\"logouttime\":\"01/01/2020\"}],\"location\":[{\"locationid\":1,\"patientid\":[],\"location\":\"BAngalore\",\"longitude\":\"25.23.45e\",\"altitude\":\" 35.12.67n\"}],\"testReport\":[],\"date\":[{\"dateId\":null,\"date\":\"07-12-1997\",\"patient\":[]}]"
//               ));
//    }
//}