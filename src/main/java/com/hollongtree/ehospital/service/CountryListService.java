package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.CountryListDao;
import com.hollongtree.ehospital.entity.CountryList;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
public class CountryListService {
	
	@Autowired
	CountryListDao countryListDao;

	/**
	 * Method for saving the data in the database
	 * @param countryList
	 * @return
	 */
	public CountryList addCountryList(CountryList countryList) {
		
		CountryList cl=countryListDao.save(countryList);
		return cl;
		
	}
	/**
	 * Method for updating the record
	 * @param countryList
	 * @return
	 * @throws EhospitalException 
	 */

	public CountryList updateCountryList(CountryList countryList,int countryId) throws EhospitalException {
		
		CountryList cl=countryListDao.findById(countryId).orElseThrow(()-> new EhospitalException("countryId not found"));
		cl.setCountryimagepath(countryList.getCountryimagepath());
		cl.setCountryname(countryList.getCountryname());
		cl.setState_id(countryList.getState_id());
		
		CountryList cl1=countryListDao.save(cl);
		return cl1;
		
	}

	/**
	 * Method for fetching the data, if not found then throw a exception
	 * @param countryName
	 * @return
	 * @throws EhospitalException
	 */
	public CountryList addCountryList(String countryName) throws EhospitalException {
		CountryList cl=countryListDao.findByCountryName(countryName);
		if(cl==null)
		{
			throw new EhospitalException("Country not found");
		}
		return cl;
	}

}
