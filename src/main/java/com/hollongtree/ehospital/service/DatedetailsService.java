//package com.hollongtree.ehospital.service;
//
//import com.hollongtree.ehospital.EhospitalApplication;
//import com.hollongtree.ehospital.Repository.DateRepository;
//import com.hollongtree.ehospital.Repository.DoctorDetailsDao;
//import com.hollongtree.ehospital.Repository.PatientRepository;
//import com.hollongtree.ehospital.entity.Datedetails;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.Date;
//import java.util.List;
//
//@Service
//public class DatedetailsService {
//    private static final Logger logger = LogManager.getLogger(EhospitalApplication.class);
//
//
//    @Autowired
//    DateRepository dateRepository;
//
//    @Autowired
//    PatientRepository patientRepository;
//
//    @Autowired
//    DoctorDetailsDao doctorDetailsDao;
//
//    public Datedetails getDatedetailsForPatient(Date date){
//        logger.info(" getDatedetailsForPatient methods starts for patient ");
//
//        Datedetails datedetails = dateRepository.findBydate(date);
//        logger.info(" getDatedetailsForPatient methods ends by returning datedetails for patient ");
//
//        return datedetails;
//    }
//
//
//    public List<Datedetails> getDatedetailsForALLPatient() {
//        logger.info(" getDatedetailsForALLPatient methods  returns list of datedetails for patient ");
//
//        return dateRepository.findAll();
//    }
//
//  // Deatils for doctor
////
////    public Datedetails getDatedetailsForDoctor(Date date){
////        logger.info(" getDatedetailsForPatient methods starts for doctor ");
////
////
////        Datedetails datedetails = dateRepository.findBydate(date);
////        logger.info(" getDatedetailsForPatient methods ends by returning datedetails for doctor ");
////
////        return datedetails;
////    }
////
////
////    public List<Datedetails> getDatedetailsForALLDoctor() {
////        logger.info(" getDatedetailsForALLPatient methods  returns list of datedetails for doctor ");
////
////        return dateRepository.findAll();
////    }
////
//}
