package com.hollongtree.ehospital.service;

import com.hollongtree.ehospital.Repository.HospitalCategoryDao;
import com.hollongtree.ehospital.entity.HospitalCategory;
import com.hollongtree.ehospital.exception.HospitalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.hollongtree.ehospital.Repository.HospitalSubcategoryDiseaseDao;
import com.hollongtree.ehospital.entity.HospitalSubcategoryDisease;
import com.hollongtree.ehospital.exception.EhospitalException;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.rowset.serial.SerialException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class HospitalSubcategoryDiseaseService {
	@Autowired
	HospitalSubcategoryDiseaseDao hospitalSubcategoryDiseaseDao;

	@Autowired
	HospitalCategoryDao hospitalCategoryDao;

	/**
	 * save the data to the reporsitory
	 *
	 * @param hospitalSubcategoryDisease
	 * @return returning the entity
	 */
	public HospitalSubcategoryDisease addHospitalSubcategoryDisease(
			HospitalSubcategoryDisease hospitalSubcategoryDisease) {

		HospitalSubcategoryDisease hsd = hospitalSubcategoryDiseaseDao.save(hospitalSubcategoryDisease);

		return hsd;
	}

	/**
	 * logic for update the HospitalSubcategoryDisease entity
	 *
	 * @param hospitalSubcategoryDisease
	 * @return
	 * @throws EhospitalException
	 */

	public HospitalSubcategoryDisease updateHospitalSubcategoryDisease(
			@RequestBody HospitalSubcategoryDisease hospitalSubcategoryDisease, int hospitalSubcategoryId) throws EhospitalException {
		HospitalSubcategoryDisease hsd = hospitalSubcategoryDiseaseDao.findById(hospitalSubcategoryId).orElseThrow(() -> new EhospitalException("hospitalSubcategoryId not found"));
		hsd.setHospitalsubcategoryimage_path(hospitalSubcategoryDisease.getHospitalsubcategoryimage_path());
		hsd.setHospitalsubcategoryname(hospitalSubcategoryDisease.getHospitalsubcategoryname());
		HospitalSubcategoryDisease hsd1 = hospitalSubcategoryDiseaseDao.save(hsd);
		return hsd1;
	}

	/**
	 * fetching the data using HospitalSubcategoryname
	 * if not present then throwing a exception message
	 *
	 * @param hospitalSubcategoryName
	 * @return
	 * @throws EhospitalException
	 */
//	public HospitalSubcategoryDisease getHospitalSubcategoryDisease(String hospitalSubcategoryName) throws EhospitalException {
//		HospitalSubcategoryDisease hsd = hospitalSubcategoryDiseaseDao
//				.findByhospitalSubcategoryName(hospitalSubcategoryName);
//		if (hsd == null) {
//			throw new EhospitalException("hospitalSubcategory Disease Name not found");
//		}
//		return hsd;
//	}

	public List<HospitalSubcategoryDisease> getAllHsc() {
		return hospitalSubcategoryDiseaseDao.findAll();
	}


//	public HospitalSubcategoryDisease updateHosSubcat(MultipartFile file, int hospitalSubcategoryId) throws EhospitalException, IOException, SerialException, SQLException {
//
//		HospitalSubcategoryDisease hsc = hospitalSubcategoryDiseaseDao.findById(hospitalSubcategoryId).orElseThrow(() -> new EhospitalException("hi"));
//		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
//		hsc.setType(file.getContentType());
//		hsc.setImage(file.getBytes());
//		hsc.setName(fileName);
//
//
//		HospitalSubcategoryDisease lt = hospitalSubcategoryDiseaseDao.save(hsc);
//		return lt;
//	}
/////

	public HospitalSubcategoryDisease find(Integer hospitalcategoryid) throws EhospitalException {
		Optional<HospitalSubcategoryDisease> hsd = hospitalSubcategoryDiseaseDao.
				findById(hospitalcategoryid);


		if (hsd.isPresent()) {
			return hsd.get();

		}
		return hsd.get();
	}


	public HospitalSubcategoryDisease getHossubcatid(Integer hospitalcategoryid)  {

		Optional<HospitalSubcategoryDisease> hossubcat= hospitalSubcategoryDiseaseDao.findById(hospitalcategoryid);

		return hospitalSubcategoryDiseaseDao.findById(hospitalcategoryid).orElse(null);

	}


	public List<HospitalSubcategoryDisease> getHscatByHsatId(Integer hospitalCategoryId) throws EhospitalException {
		List<HospitalSubcategoryDisease> dd1 = hospitalSubcategoryDiseaseDao.fetchHsubcatBysHcatId(hospitalCategoryId);
		if (dd1 == null) {
			throw new EhospitalException("Id not found");

		}

		return dd1;
	}


	public HospitalSubcategoryDisease getHscatForPatient(Integer testReportid)  {

		Optional<HospitalSubcategoryDisease> Hscat= hospitalSubcategoryDiseaseDao.findById(testReportid);

		return hospitalSubcategoryDiseaseDao.findById(testReportid).get();

	}

	public List<HospitalSubcategoryDisease> getHscatByHsatName(HospitalCategory hospitalCategoryName) throws EhospitalException {
		List<HospitalSubcategoryDisease> Hn = hospitalSubcategoryDiseaseDao.fetchHsubcatByHscatName(hospitalCategoryName);
		if (Hn == null) {
			throw new EhospitalException("Name not found");

		}

		return Hn;
	}

	public List<HospitalSubcategoryDisease> getHscatByHsatId(HospitalCategory hospitalCategoryId) throws EhospitalException {
		List<HospitalSubcategoryDisease> Hn = hospitalSubcategoryDiseaseDao.fetchHsubcatByHscatId(hospitalCategoryId);
		if (Hn == null) {
			throw new EhospitalException("Name not found");

		}

		return Hn;
	}

//	public HospitalSubcategoryDisease savehossubcat(HospitalSubcategoryDisease hospitalSubcategoryDisease,Integer hospitalCategoryId  ) throws HospitalException
//	{
//
//		Optional< HospitalCategory> hospitalCategory=  hospitalCategoryDao.findById(hospitalCategoryId);
//
//       if(hospitalCategory.isPresent()){
//		   hospitalSubcategoryDisease.getHospitalsubcategoryname(hospitalCategory.get());
//		   hospitalSubcategoryDiseaseDao.save(hospitalSubcategoryDisease);
//        }
//        else {
//            throw new HospitalException("Patient does not exist with id: "+ hospitalCategoryId);
//        }
//
//		return hospitalSubcategoryDiseaseDao.save(hospitalSubcategoryDisease);
//
//	}





}



