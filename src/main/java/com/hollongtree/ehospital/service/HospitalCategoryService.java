package com.hollongtree.ehospital.service;

import static org.assertj.core.api.Assertions.catchThrowable;

import com.hollongtree.ehospital.Repository.HospitalSubcategoryDiseaseDao;
import com.hollongtree.ehospital.entity.HospitalCatalogue;
import com.hollongtree.ehospital.entity.HospitalSubcategoryDisease;
import com.hollongtree.ehospital.exception.HospitalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;

import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.Repository.HospitalCategoryDao;
import com.hollongtree.ehospital.entity.HospitalCategory;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.rowset.serial.SerialException;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Service
public class HospitalCategoryService {

	@Autowired
	HospitalCategoryDao hospitalCategoryDao;

	@Autowired
	HospitalSubcategoryDiseaseDao hospitalSubcategoryDiseaseDao;

	/**
	 * save the data to the reporsitory
	 *
	 * @param hospitalCategory
	 * @return
	 */
	public HospitalCategory addHospitalCategory(HospitalCategory hospitalCategory) {

		HospitalCategory hc = hospitalCategoryDao.save(hospitalCategory);

		return hc;

	}

	/**
	 * Update the HospitalCategory entity
	 *
	 * @param hospitalCategory
	 * @return
	 * @throws EhospitalException
	 */
	public HospitalCategory updateHospitalCategory(@RequestBody HospitalCategory hospitalCategory,int hospitalCategoryId) throws EhospitalException {

		HospitalCategory hc=hospitalCategoryDao.findById(hospitalCategoryId).orElseThrow(()-> new EhospitalException("hospitalCategoryId not found"));
//		hc.setHospitalSubcategoryDisease(hospitalCategory.getHospitalSubcategoryDisease());
		hc.setHospitalcategoryname(hospitalCategory.getHospitalcategoryname());
		hc.setHospitalcategorypath(hospitalCategory.getHospitalcategorypath());
		HospitalCategory hc1 = hospitalCategoryDao.save(hc);
		return hc1;

	}

	/**
	 * fetching the values from HospitalCategory using HospitalCategoryName if not
	 * found then throw a exception
	 *
	 * @param hospitalCategoryName
	 * @return
	 * @throws EhospitalException
	 */

	public List<HospitalCategory> getHossubCategory(String hospitalCategoryName) throws EhospitalException {

		List<HospitalCategory> hc = hospitalCategoryDao.findByhospitalCategoryName(hospitalCategoryName);
		if (hc == null) {
			throw new EhospitalException("hospitalCategoryName not found");
		}
		return hc;


	}
	//FileInputStream fileInputStream = new FileInputStream(image);
	public List<HospitalCategory> getAllHos() {
		return hospitalCategoryDao.findAll();
	}


	public HospitalCategory updateHos(MultipartFile file, int hospitalcategoryid) throws  EhospitalException, IOException, SerialException, SQLException {

		HospitalCategory hc=hospitalCategoryDao.findById(hospitalcategoryid).orElseThrow(()-> new EhospitalException("hi"));
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		hc.setType(file.getContentType());
		hc.setImage(file.getBytes());
		hc.setName(fileName);


		HospitalCategory lt=hospitalCategoryDao.save(hc);
		return lt;
	}

	public List<HospitalCategory> getCategoryForProducts(Integer hospitalCategoryId)throws EhospitalException  {

		List<HospitalCategory> Hc= hospitalCategoryDao.findBycatId(hospitalCategoryId);

		if (Hc == null) {
			throw new EhospitalException("hospitalCategoryName not found");
		}
		return Hc;

	}
//	public HospitalCategory savehossubcat(HospitalCategory hospitalCategory,Integer hospitalCategoryId  ) throws HospitalException
//	{
//
//		Optional< HospitalCategory> hospitalCategory1=  hospitalCategoryDao.findById(hospitalCategoryId);
//
//		if(hospitalCategory1.isPresent()){
//			hospitalCategory.(hospitalCategoryId);
//			hospitalCategoryDao.save(hospitalCategory);
//		}
//		else {
//			throw new HospitalException("Patient does not exist with id: "+ hospitalCategoryId);
//		}
//
//		return hospitalCategoryDao.save(hospitalCategory);
//
//	}


	public HospitalCategory savehossubcat(HospitalCategory hospitalCategory) {
		HospitalCategory hc=new HospitalCategory();
		hc=hospitalCategoryDao.save(hospitalCategory);


		return hc;
	}



}
