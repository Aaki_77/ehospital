package com.hollongtree.ehospital.service;


import com.hollongtree.ehospital.Repository.PageRepository;
import com.hollongtree.ehospital.entity.HealthFile;
import com.hollongtree.ehospital.entity.PrescriptionPage;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class PageService {

    @Autowired
    PageRepository pageRepository;

    public PrescriptionPage savePage(PrescriptionPage page){
        return pageRepository.save(page);
    }
    
//    public HealthFile store(MultipartFile file) throws IOException {
//		
//    	String fileName = StringUtils.cleanPath(file.getOriginalFilename());
//    	PrescriptionPage FileDB = new PrescriptionPage(fileName, file.getContentType(), file.getBytes());
//    		//file.transferTo(new File("C:\\Users\\nishi\\Downloads\\New folder (3)\\Aaki_77-ehospital-hybrid-d83740ad4d13\\public\\images\\HealthFile\\"+file.getOriginalFilename()));
//
//    		return healthFileRepo.save(FileDB);
//    	}

	public PrescriptionPage getPage(String patientid) {
		
		PrescriptionPage prescriptionPage=pageRepository.fetchPatient(patientid);
		
		return prescriptionPage;
	}

	public List<PrescriptionPage> getPatientPage() {
		List<PrescriptionPage> pg=pageRepository.findAll();
		return pg;
		 
	}

	public PrescriptionPage store(MultipartFile file) throws IOException {
		
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		PrescriptionPage FileDB = new PrescriptionPage(fileName, file.getContentType(), file.getBytes());
			//file.transferTo(new File("C:\\Users\\nishi\\Downloads\\New folder (3)\\Aaki_77-ehospital-hybrid-d83740ad4d13\\public\\images\\HealthFile\\"+file.getOriginalFilename()));

			return pageRepository.save(FileDB);
		}


}
