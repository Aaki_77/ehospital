package com.hollongtree.ehospital.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.AdminLoginDao;
import com.hollongtree.ehospital.entity.AdminLogin;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
//public class AdminLoginService implements UserDetailsService {
	public class AdminLoginService{
	@Autowired
	AdminLoginDao adminLoginDao;

	public AdminLogin addAdminLogin(String email, String password) throws EhospitalException {
		AdminLogin login = adminLoginDao.findByemail(email);
		
		if (login == null)
		{
			throw new EhospitalException("invalid email");
		}
		else
		{
			if(!(login.getPassword().equals(password)))
			{
				throw new EhospitalException("invalid password");
			}
		}
 
		
		return login;
	}

	public AdminLogin updateAdminLogin(AdminLogin login, int loginId) throws EhospitalException {

		AdminLogin log= adminLoginDao.findById(loginId).orElseThrow(()-> new EhospitalException("loginId not found"));
		log.setEmail(login.getEmail());
		log.setName(login.getName());
		log.setPassword(login.getPassword());

		AdminLogin log1 = adminLoginDao.save(log);

		return log1;
	}

//	@Override
//	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
//		AdminLogin login=adminLoginDao.findByemail(email);
//		if(login==null)
//		{
//			throw new UsernameNotFoundException(email+"not found");
//		}
//		return new User(login.getEmail(),login.getPassword(),new ArrayList<>());
//	}

	

	

}
