package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.HospitalCatalogueDao;
import com.hollongtree.ehospital.entity.HospitalCatalogue;
import com.hollongtree.ehospital.exception.EhospitalException;

import java.io.FileInputStream;
import java.util.List;

@Service
public class HospitalCatalogueService {

	@Autowired
	HospitalCatalogueDao hospitalCatalogueDao;

	/**
	 * Save the data to the reporsitory
	 *
	 * @param hospitalcatalogue
	 * @return
	 */
	public HospitalCatalogue addHospitalCatalogue(HospitalCatalogue hospitalcatalogue) {

		HospitalCatalogue hc = hospitalCatalogueDao.save(hospitalcatalogue);
		return hc;
	}

	/**
	 * Update the HospitalCatalogue Entity
	 *
	 * @param hospitalCatalogue
	 * @return
	 * @throws EhospitalException
	 */

	public HospitalCatalogue updateHospitalCatalogue(HospitalCatalogue hospitalCatalogue,int hospitalCatalogueId) throws EhospitalException {

		HospitalCatalogue hcat= hospitalCatalogueDao.findById(hospitalCatalogueId).orElseThrow(()-> new EhospitalException("hospitalCatalogueId not found"));

		hcat.setCatalogueimagepath(hospitalCatalogue.getCatalogueimagepath());
		hcat.setCataloguename(hospitalCatalogue.getCataloguename());
		HospitalCatalogue hcat1 = hospitalCatalogueDao.save(hcat);

		return hcat1;

	}

	/**
	 * fetching value from HospitalCatalogue using catalogueName if not then throw a
	 * exception
	 *
	 * @param catalogueName
	 * @return
	 * @throws EhospitalException
	 */

	public HospitalCatalogue getHospitalCatalogue(String catalogueName) throws EhospitalException {
		HospitalCatalogue hc = hospitalCatalogueDao.findByCatalogueName(catalogueName);
		if (hc == null) {
			throw new EhospitalException("Hospital catalogue not found");
		}
		return hc;
	}
	public List<HospitalCatalogue> getAllHC() {
		return hospitalCatalogueDao.findAll();
	}



}
