package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.MedicalListDao;
import com.hollongtree.ehospital.entity.MedicalList;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
public class MedicalListService {
	
	@Autowired
	MedicalListDao medicalListDao;

	/**
	 * method for Save the medicalList data
	 * @param medicalList
	 * @return
	 */
	public MedicalList addMedicalList(MedicalList medicalList) {
		MedicalList ml=medicalListDao.save(medicalList);
		return ml;
	}

	/**
	 * method for Update the medicalList entity
	 * @param medicalList
	 * @return
	 * @throws EhospitalException 
	 */
	public MedicalList updateMedicalList(MedicalList medicalList,int medicalId) throws EhospitalException {
		
		MedicalList ml=medicalListDao.findById(medicalId).orElseThrow(()-> new EhospitalException("medicalId not found"));
		ml.setCityId(medicalList.getCityId());
		ml.setCountryId(medicalList.getCountryId());
		ml.setHospitalId(medicalList.getHospitalId());
		ml.setLocation(medicalList.getLocation());
		ml.setMedicalAddress1(medicalList.getMedicalAddress1());
		ml.setMedicalAddress2(medicalList.getMedicalAddress2());
		ml.setMedicalName(medicalList.getMedicalName());
		ml.setPincode(medicalList.getPincode());
		ml.setStateId(medicalList.getStateId());
		
		MedicalList ml1=medicalListDao.save(ml);
		return ml1;
	}

	/**
	 * method for fetching the medicallist using medicalname if not found then throw a exception
	 * @param medicalName
	 * @return
	 * @throws EhospitalException
	 */
	public MedicalList getMedicalList(String medicalName) throws EhospitalException {
		MedicalList ml=medicalListDao.findByMedicalName(medicalName);
		if(ml==null)
		{
			throw new EhospitalException("medical name not found");
		}
		return ml;
	}
	

}
