package com.hollongtree.ehospital.service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.RegistrationRepository;
//import com.hollongtree.ehospital.entity.MobileNum;
import com.hollongtree.ehospital.entity.Registration;

@Service
public class RegistrationService {
	
	@Autowired
	RegistrationRepository registrationRepository;



    public Registration saveRegistration(Registration registration){

    	registrationRepository.save(registration);
       // logger.info("Save patient starts");
        return registration;
    }

    public Registration fetchRegistration(Integer RegistrationId){

       // logger.info("fetch method for patient starts");

        Optional<Registration> optionalRegistration= registrationRepository.findById(RegistrationId);


        if(optionalRegistration.isPresent()){

            Registration registration= optionalRegistration.get();

	
	
//	 Set<MobileNum> mobileNums=registration.getMobile();
//     
//
//
//     Set<MobileNum> mobileNumsRes=new HashSet<>();
//
//     for(MobileNum mobileNum : mobileNums){
//
//     	MobileNum mobileNumObject= new MobileNum();
//     	mobileNumObject.setNumber(mobileNum.getNumber());
//
//     	mobileNumsRes.add(mobileNumObject);
//
//
//     }
//
//     registration.setMobile(mobileNumsRes);

     
     return registration;

}
		return null;
}

	
}