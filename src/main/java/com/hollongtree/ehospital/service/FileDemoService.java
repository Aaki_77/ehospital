package com.hollongtree.ehospital.service;

import com.hollongtree.ehospital.Repository.FileDemoRepository;
import com.hollongtree.ehospital.Repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class FileDemoService {

    @Autowired
    FileDemoRepository fileDemoRepository;

    @Autowired
    PatientRepository patientRepository;

    public static void main(String args[]) throws IOException {


        Scanner reader = new Scanner(System.in);
        boolean success = false;



        System.out.println("Enter path of directory to create");
        String dir = reader.nextLine();


        // Creating new directory in Java, if it doesn't exists
        File directory = new File(dir);
        if (directory.exists() ) {
            System.out.println("Directory already exists ...");

        } else {
            System.out.println("Directory not exists, creating now");

            success = directory.mkdir();
            if (success) {
                System.out.printf("Successfully created new directory : %s%n", dir);
            } else {
                System.out.printf("Failed to create new directory: %s%n", dir);
            }
        }


        // Creating new file in Java, only if not exists
        System.out.println("Enter Prescription file name to be created ");
        String filename = reader.nextLine();

        System.out.println("Enter patient id");
        int patient=reader.nextInt();

        File f = new File(filename);
        if (f.exists() || patient ==1 ) {
            System.out.println("File already exists");

        } else {
            System.out.println("No such file exists, creating now");
            success = f.createNewFile();
            if (success) {
                System.out.printf("Successfully created new Prescription file: %s%n", f);
            } else {
                System.out.printf("Failed to create new Prescription file: %s%n", f);
            }
        }

        // close Scanner to prevent resource leak
        reader.close();

    }
}
