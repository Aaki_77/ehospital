package com.hollongtree.ehospital.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.DoctorDetailsDao;
import com.hollongtree.ehospital.entity.DoctorDetails;
import com.hollongtree.ehospital.exception.EhospitalException;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.rowset.serial.SerialException;

@Service
public class DoctorDetailsService {

	@Autowired
	DoctorDetailsDao doctorDetailsDao;

	/**
	 * Method for saving the data to the databasedoctor_details
	 *
	 * @param doctordetails
	 * @return
	 */
	public DoctorDetails addDoctorDetails(DoctorDetails doctordetails) {

		DoctorDetails dd = doctorDetailsDao.save(doctordetails);
		return dd;

	}

	/**
	 * Method for Updating the record
	 *
	 * @param doctordetails
	 * @return
	 * @throws EhospitalException
	 */

	public DoctorDetails updateDoctorDetails(DoctorDetails doctordetails,int doctorDetailsId) throws EhospitalException {

		DoctorDetails dd=doctorDetailsDao.findById(doctorDetailsId).orElseThrow(()-> new EhospitalException("doctorDetailsId not found"));;

		dd.setActiveTime(doctordetails.getActiveTime());
		dd.setAggrementId(doctordetails.getAggrementId());
		dd.setCharges(doctordetails.getCharges());
		dd.setChargesType(doctordetails.getChargesType());
		dd.setCityId(doctordetails.getCityId());
		dd.setCountryId(doctordetails.getCityId());
		dd.setDoctorAddress(doctordetails.getDoctorAddress());
		dd.setDoctorEmail(doctordetails.getDoctorEmail());
		dd.setDoctorFee(doctordetails.getDoctorFee());
		dd.setDoctorName(doctordetails.getDoctorName());
		dd.setDoctorPassword(doctordetails.getDoctorPassword());
		dd.setEndTime(doctordetails.getEndTime());
		dd.setFromTime(doctordetails.getFromTime());
		dd.setHospitalSubcategoryId(doctordetails.getHospitalSubcategoryId());
		dd.setImagePath(doctordetails.getImagePath());
		dd.setNoOfPatient(doctordetails.getNoOfPatient());
		dd.setOnePatientsValidity(doctordetails.getOnePatientsValidity());
		dd.setPincode(doctordetails.getPincode());
		dd.setStateId(doctordetails.getStateId());
		dd.setUdf1(doctordetails.getUdf1());
		dd.setUdf2(doctordetails.getUdf2());
		dd.setUdf3(doctordetails.getUdf3());
		dd.setUdf4(doctordetails.getUdf4());
		dd.setUdf5(doctordetails.getUdf5());


		DoctorDetails dd1 = doctorDetailsDao.save(dd);
		return dd1;

	}

	/**
	 * Method for fetching the data from database if not found then throw a
	 * exception
	 *
	 * @param doctorname
	 * @return
	 * @throws EhospitalException
	 */

	public DoctorDetails getDoctorDetailsByDoctorName(String doctorName) throws EhospitalException {
		DoctorDetails dd = doctorDetailsDao.findByDoctorname(doctorName);
		if (dd == null) {
			throw new EhospitalException("doctor name not found");
		}
		return dd;
	}

	public List<DoctorDetails> getDoctorDetails() {
		List<DoctorDetails> doctorDetails1=doctorDetailsDao.findAll();
		return doctorDetails1;
	}

	public DoctorDetails getDoctorDetailsByPatientId(String patientid) {
		DoctorDetails dd= doctorDetailsDao.fetchDoctorByPatientid(patientid);
		return dd;
	}

	public DoctorDetails doctorDetails(MultipartFile file, int hospitalSubcategoryId) throws  EhospitalException, IOException, SerialException, SQLException {

		DoctorDetails Dd=doctorDetailsDao.findById(hospitalSubcategoryId).orElseThrow(()-> new EhospitalException("hi"));
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		Dd.setType(file.getContentType());
		Dd.setImage(file.getBytes());
		Dd.setName(fileName);


		DoctorDetails lt=doctorDetailsDao.save(Dd);
		return lt;
	}

	public List<DoctorDetails> getDoctorDetailsBysubcatId(Integer hospitalSubcategoryId) throws EhospitalException {
		List<DoctorDetails> dd1 = doctorDetailsDao.fetchDoctorDetailsBysubcatId(hospitalSubcategoryId);
//        dd1.set(hospitalCategoryId,);
		if (dd1 == null) {
			throw new EhospitalException("Id not found");

		}

		return dd1;
	}


	public List<DoctorDetails> getDoctorDetailsBysubcatId2(Integer hospitalCategoryId,Integer hospitalSubcategoryId) throws EhospitalException {
		List <DoctorDetails> dd2= doctorDetailsDao.fetchDoctorDetailsBysubcatId2(hospitalSubcategoryId);

		if (dd2 == null) {
			throw new EhospitalException("Id not found");

		}

		return dd2;
	}

//	public DoctorDetails getDescriptionDetailsByDoctorName(Integer hospitalSubcategoryId) throws EhospitalException {
//		DoctorDetails dd4 = doctorDetailsDao.findById(hospitalSubcategoryId);
//		if (dd4 == null) {
//			throw new EhospitalException("doctor name not found");
//		}
//		return dd4;
//	}




	public List<DoctorDetails> getAlllist() {
		DoctorDetails dy=doctorDetailsDao.saveall();
		return null;
	}


//	public DoctorDetails getDoctorForPatient(Integer doctorDetailsId)  {
//
//		Optional<DoctorDetails> DD= doctorDetailsDao.findById(doctorDetailsId);
//
//		return doctorDetailsDao.findById(doctorDetailsId).get();
//
//	}

	public List<DoctorDetails> getDoctorDetailsById(Integer doctorDetailsId) throws EhospitalException {
		List<DoctorDetails> DDD = doctorDetailsDao.findByDoctorId(doctorDetailsId);
		if (DDD == null) {
			throw new EhospitalException("doctor name not found");
		}
		return DDD;
	}


	public List<DoctorDetails> getDoctorDetailsBysubcatId11(Integer hospitalCategoryId,Integer hospitalSubcategoryId) throws EhospitalException {
		List <DoctorDetails> dd11= doctorDetailsDao.fetchDoctorDetailsBysubcatId11(hospitalSubcategoryId);

		if (dd11 == null) {
			throw new EhospitalException("Id not found");

		}

		return dd11;
	}
}
