package com.hollongtree.ehospital.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import com.hollongtree.ehospital.Repository.MedicineListDao;
import com.hollongtree.ehospital.entity.MedicineList;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
public class MedicineListService {
	
	@Autowired
	MedicineListDao medicineListDao;

	/**
	 * Method for saving the data
	 * @param medicineList
	 * @return
	 */
	public MedicineList addMedicineList(MedicineList medicineList) {
		
		MedicineList ml=medicineListDao.save(medicineList);
		return ml;
		
	}

	/**
	 * Method for updating medicinelist entity
	 * @param medicineList
	 * @return
	 * @throws EhospitalException 
	 */
	public MedicineList updateMedicineList(MedicineList medicineList,int medicineId) throws EhospitalException {
		
		MedicineList ml= medicineListDao.findById(medicineId).orElseThrow(()-> new EhospitalException("medicineId not found"));
		ml.setMedicalList(medicineList.getMedicalList());
		ml.setMedicineDescription(medicineList.getMedicineDescription());
		ml.setMedicineName(medicineList.getMedicineName());
		ml.setUdf1(medicineList.getUdf1());
		ml.setUdf2(medicineList.getUdf2());
		ml.setUdf3(medicineList.getUdf3());
		ml.setUdf4(medicineList.getUdf4());
		ml.setUdf5(medicineList.getUdf5());
		ml.setUdf6(medicineList.getUdf6());
		ml.setUdf7(medicineList.getUdf7());
		MedicineList ml1=medicineListDao.save(ml);
		return ml1;
		
	}

	/**
	 * Method for fetching the data using medicine name, if not found then throw a Exception
	 * @param medicineName
	 * @return
	 * @throws EhospitalException
	 */
	public MedicineList getMedicineList(String medicineName) throws EhospitalException {
		MedicineList ml=medicineListDao.findByMedicineName(medicineName);
		if(ml==null)
		{
			throw new EhospitalException("medicine name not found");
		}
		return ml;
	}

	public List<MedicineList> getMedicineListAll() {
		List<MedicineList> ml=medicineListDao.findAll();
		return ml;
	}
	
	public List<MedicineList> getMedicineList(int medicineId) {
		
		List<MedicineList> ml=medicineListDao.findByMedicineId(medicineId);
		return ml;
	}

	public MedicineList updateMedicineListImage(MultipartFile file, int medicineId) throws EhospitalException, IOException {
		MedicineList ml=medicineListDao.findById(medicineId).orElseThrow(()->new EhospitalException("medicineId not found"));
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		ml.setType(file.getContentType());
		ml.setMedicineImage(file.getBytes());
		ml.setName(fileName);
		MedicineList mlr=medicineListDao.save(ml);
		return mlr;
	}

	

}
