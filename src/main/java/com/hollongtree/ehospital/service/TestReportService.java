package com.hollongtree.ehospital.service;


import com.hollongtree.ehospital.Repository.TestReportRepository;
import com.hollongtree.ehospital.entity.TestReport;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestReportService {

    @Autowired
    TestReportRepository testReportRepository;

    public TestReport saveTestReport(TestReport testReport){
        TestReport tr=testReportRepository.save(testReport);
        return tr;
    }

	public List<TestReport> getAllTest() {
		return testReportRepository.findAll();
	}


}
