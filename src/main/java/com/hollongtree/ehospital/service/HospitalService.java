package com.hollongtree.ehospital.service;

import com.hollongtree.ehospital.Repository.HospitalRepository;
import com.hollongtree.ehospital.entity.Hospital;
import com.hollongtree.ehospital.exception.EhospitalException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class HospitalService {

    @Autowired
    HospitalRepository hospitalRepository;

    /**Saving the hospital data to the reporsitory
     * 
     * @param hospital
     * @return
     */
    public Hospital saveHospital(Hospital hospital){
    	
        return hospitalRepository.save(hospital);
    }

    /**fetching data using hospitalname
     * if not present then throwing a exception
     * @param hospitalName
     * @return returning the hospital entity
     * @throws EhospitalException
     */
	public Hospital getHospital(String hospitalName) throws EhospitalException   {
		
		
		Hospital h=hospitalRepository.findByHospitalname(hospitalName);
		if(h==null)
		{
			throw new EhospitalException("hospital not found");
		}
		
		return h;
	}
	/**logic for updating the hospital values
	 * 
	 * @param hospital
	 * @return
	 * @throws EhospitalException 
	 */

	public Hospital updateHospital(Hospital hospital,int hospitalId) throws EhospitalException {

		Hospital h=hospitalRepository.findById(hospitalId);
		h.setHospitalAddress(hospital.getHospitalAddress());
		h.setHospitalCatalogue(hospital.getHospitalCatalogue());
		h.setHospitalImagePath(hospital.getHospitalImagePath());
		h.setHospitalName(hospital.getHospitalName());
		Hospital h1=hospitalRepository.save(h);
		return h1;
		
	}
}
