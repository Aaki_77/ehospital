package com.hollongtree.ehospital.service;

import com.hollongtree.ehospital.Repository.LabtestListRepository;
import com.hollongtree.ehospital.entity.CountryList;
import com.hollongtree.ehospital.entity.LabTestList;
import com.hollongtree.ehospital.exception.EhospitalException;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Service
public class LabtestListService {

    @Autowired
    LabtestListRepository labtestRepository;

    public LabTestList saveLabtest(LabTestList labTestList) throws IOException{
    
    	//LabTestList ltl = new LabTestList();
    	//String fileName = StringUtils.cleanPath(file.getOriginalFilename());
    	//ltl.setTestImage(Base64.getEncoder().encodeToString(file.getBytes()));
    	LabTestList l1=labtestRepository.save(labTestList);
    	
        return l1;
    }

//	public Stream<LabTestList> getLabTest(int labTestListId) throws EhospitalException {
//		
//		Stream<LabTestList> lbt=labtestRepository.findById(labTestListId).stream();
//		System.out.println(lbt);
//		if(lbt==null)
//		{
//			throw new EhospitalException("labTestListId not found");
//		}
//		return lbt;
//	}

	public Stream<LabTestList> getLabTestBy(int hospitalId) throws EhospitalException {
		Stream<LabTestList> lbt=labtestRepository.findByHospitalId(hospitalId).stream();
		if(lbt==null)
		{
			throw new EhospitalException("labTestListId not found");
		}
		return lbt;
	}
	

	public LabTestList updateLabTest(MultipartFile file,int labTestListId) throws  EhospitalException, IOException, SerialException, SQLException {
		
		LabTestList l=labtestRepository.findById(labTestListId).orElseThrow(()-> new EhospitalException("hi"));
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		l.setType(file.getContentType());
		l.setImage(file.getBytes());
		l.setName(fileName);
		

		LabTestList lt=labtestRepository.save(l);
		return lt;
	}

	public Stream<LabTestList> getAllFiles() {
		Stream<LabTestList> ltl=labtestRepository.findAll().stream();
		
		
		return ltl;
	}

	public List<LabTestList> getAllLabTest() {
		List<LabTestList> ltl=labtestRepository.findAll();
		return ltl;
	}

	
	
	
	
	
	
}
