package com.hollongtree.ehospital.entity;


import javax.persistence.*;

@Entity
@Table(name = "Documents")
public class Filedemo {

    private int fileId;

    private Patient patient;


    @Id
    @Column(name = "documents_id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "patient_id")
    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
