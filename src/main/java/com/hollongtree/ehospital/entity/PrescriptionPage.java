package com.hollongtree.ehospital.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "page")
public class PrescriptionPage {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="page_id", unique= true, nullable =false)
	private Integer pageid;

	private String name;

	private String type;

	@Lob
	private byte[] data;
	
	//private Set<PrescriptionList> prescriptionset = new HashSet<PrescriptionList>(0);

	public PrescriptionPage() {
	}

	public PrescriptionPage(String name, String type, byte[] data) {
		this.name = name;
		this.type = type;
		this.data = data;
	}
	

public Integer getPageid() {
	return pageid;
}

//public void setPageid(Integer pageid) {
//	this.pageid = pageid;

//	public int getPageId() {
//		return pageid;
//	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}


	
	
}






//
//
//
//@Id
//@GeneratedValue(generator = "uuid")
//@GenericGenerator(name = "uuid", strategy = "uuid2")
//private String id;
//	
//private String name;
//
//private String type;
//
//@Lob
//private byte[] data;
//
////private Set<PrescriptionList> prescriptionset = new HashSet<PrescriptionList>(0);
//
//public PrescriptionPage() {
//}
//
//
//
//
//public PrescriptionPage(String id, String name, String type, byte[] data) {
//	super();
//	this.id = id;
//	this.name = name;
//	this.type = type;
//	this.data = data;
//}
//
//
//
//
////@Id
////@GeneratedValue(strategy = GenerationType.IDENTITY)
////@Column(name="page_id", unique= true, nullable =false)
////public Integer getPageid() {
////	return pageid;
////}
////
////public void setPageid(Integer pageid) {
////	this.pageid = pageid;
////}
////
////@Column(name="page_path", nullable=false)
////public String getPagepath() {
////	return pagepath;
////}
////
////public void setPagepath(String pagepath) {
////	this.pagepath = pagepath;
////}
//
//public String getName() {
//	return name;
//}
//
//public void setName(String name) {
//	this.name = name;
//}
//
//public String getType() {
//	return type;
//}
//
//public void setType(String type) {
//	this.type = type;
//}
//
//public byte[] getData() {
//	return data;
//}
//
//public void setData(byte[] data) {
//	this.data = data;
//}
//
////@ManyToMany(fetch= FetchType.LAZY, mappedBy="pages")
////public Set<PrescriptionList> getPrescriptionset() {
////	return prescriptionset;
////}
////
////public void setPrescriptionset(Set<PrescriptionList> prescriptionset) {
////	this.prescriptionset = prescriptionset;
////}
//
//
//


