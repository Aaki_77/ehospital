package com.hollongtree.ehospital.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "medicine_image_list")
public class MedicineImageList {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "medicine_image_id")
	private int medicineImageId;
	@Column(name = "image_path")
	private String imagePath;
	@ManyToMany
	private List<MedicineList> medicineList;
	
	public MedicineImageList() {
	}

	public MedicineImageList(int medicineImageId, String imagePath, List<MedicineList> medicineList) {
		super();
		this.medicineImageId = medicineImageId;
		this.imagePath = imagePath;
		this.medicineList = medicineList;
	}

	public int getMedicineImageId() {
		return medicineImageId;
	}

	public void setMedicineImageId(int medicineImageId) {
		this.medicineImageId = medicineImageId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public List<MedicineList> getMedicineList() {
		return medicineList;
	}

	public void setMedicineList(List<MedicineList> medicineList) {
		this.medicineList = medicineList;
	}
	
	

}
