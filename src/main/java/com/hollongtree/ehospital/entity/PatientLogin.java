package com.hollongtree.ehospital.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "patient_login")
public class PatientLogin {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int loginId;
	
    @Column(name = "email",unique = true)
	private String email;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "name")
	private String name;
	
	@OneToMany(targetEntity = Patient.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "Patient_login_fk_id", referencedColumnName = "loginId")
	private List<Patient> patient;
	
	public PatientLogin() {
	}

	public PatientLogin(int loginId, String email, String password, String name, List<Patient> patient) {
		super();
		this.loginId= loginId;
		this.email = email;
		this.password = password;
		this.name = name;
		this.patient = patient;
	}

	
	public int getLoginId() {
		return loginId;
	}

	public void setLoginId(int loginId) {
		this.loginId = loginId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<Patient> getPatient() {
		return patient;
	}


	public void setHospitalCatalogue(List<Patient> patient) {
		this.patient = patient;
	}
	
		
}
