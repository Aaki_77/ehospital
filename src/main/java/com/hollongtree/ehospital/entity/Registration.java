package com.hollongtree.ehospital.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Registration")
public class Registration {
	
	private int ResgistrationId; 
	
	private String firstName;
	
	private String lastName;
	
	private Long mobileNum;
	
//	private Set<MobileNum>mobile= new HashSet<MobileNum>(0);

	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "registration_id")
	public int getResgistrationId() {
		return ResgistrationId;
	}

	public void setResgistrationId(int resgistrationId) {
		ResgistrationId = resgistrationId;
	}

	@Column(name="firstname")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name="lastname")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name="Mobile_num")
	public Long getMobileNum() {
		return mobileNum;
	}

	public void setMobileNum(Long mobileNum) {
		this.mobileNum = mobileNum;
	}
	
	
//	 @ManyToMany(cascade = CascadeType.ALL)
//	    @JoinTable(name = "Registered_Mobno", joinColumns =
//	            {@JoinColumn(name = "registration_id")},
//	            inverseJoinColumns = {@JoinColumn(name = "mobilenum_Id")})
//		public Set<MobileNum> getMobile() {
//			return mobile;
//		}
//
//		public void setMobile(Set<MobileNum> mobile) {
//			this.mobile = mobile;
//		}
	
	
	

}
