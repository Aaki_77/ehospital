package com.hollongtree.ehospital.entity;

public class PatientResponse {

    private String patientId;

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

}
