package com.hollongtree.ehospital.entity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "hospital")
public class Hospital {
	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	@GeneratedValue
	@Column(name = "hospital_id")
	private int hospitalId;

	@Column(name = "hospital_address", length = 255)
	private String hospitalAddress;

	@Column(name = "hospital_name", length = 100)
	private String hospitalName;

	@Column(name = "hospital_image_path", length = 100)
	private String hospitalImagePath;

	@OneToMany(fetch= FetchType.LAZY)
	private Set<HospitalCatalogue> hospitalCatalogue= new HashSet<>();



	public Hospital() {
	}


	public Hospital(int hospitalId, String hospitalAddress, String hospitalName, String hospitalImagePath, Set<HospitalCatalogue> hospitalCatalogue) {
		this.hospitalId = hospitalId;
		this.hospitalAddress = hospitalAddress;
		this.hospitalName = hospitalName;
		this.hospitalImagePath = hospitalImagePath;
		this.hospitalCatalogue = hospitalCatalogue;
	}

	public Integer getHospitalId() {
		return hospitalId;
	}


	public void setHospitalId(Integer hospitalId) {
		this.hospitalId = hospitalId;
	}


	public String getHospitalAddress() {
		return hospitalAddress;
	}


	public void setHospitalAddress(String hospitalAddress) {
		this.hospitalAddress = hospitalAddress;
	}


	public String getHospitalName() {
		return hospitalName;
	}


	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}


	public String getHospitalImagePath() {
		return hospitalImagePath;
	}


	public void setHospitalImagePath(String hospitalImagePath) {
		this.hospitalImagePath = hospitalImagePath;
	}



	public Set<HospitalCatalogue> getHospitalCatalogue() {
		return hospitalCatalogue;
	}

	public void setHospitalCatalogue(Set<HospitalCatalogue> hospitalCatalogue) {
		this.hospitalCatalogue = hospitalCatalogue;
	}
}