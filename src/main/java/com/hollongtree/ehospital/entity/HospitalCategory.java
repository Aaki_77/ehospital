package com.hollongtree.ehospital.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "hospital_category")
public class HospitalCategory {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "hospital_category_id")
	private int hospitalCategoryId;
	@Column(name = "hospital_category_name", length = 100)
	private String hospitalCategoryName;
	@Column(name = "hospital_category_path", length = 150)
	private String hospitalCategoryPath;

	@JsonIgnore
	@OneToMany( fetch= FetchType.LAZY,cascade=CascadeType.ALL,mappedBy = "hospitalcategory")
	private Set<HospitalSubcategoryDisease> hospitalSubcategoryDisease=new HashSet<HospitalSubcategoryDisease>();

	@OneToMany(fetch= FetchType.LAZY,cascade=CascadeType.ALL)
	private Set<DoctorDetails> doctorDetails=new HashSet<DoctorDetails>();



	@Column(name = "category_image")
	@Lob
	private byte[] image;

	private String name;

	private String type;


	public HospitalCategory() {
	}

	public HospitalCategory(String hospitalCategoryName, String hospitalCategoryPath, Set<HospitalSubcategoryDisease> hospitalSubcategoryDisease, Set<DoctorDetails> doctorDetails, byte[] image, String name, String type) {
		this.hospitalCategoryName = hospitalCategoryName;
		this.hospitalCategoryPath = hospitalCategoryPath;
		this.hospitalSubcategoryDisease = hospitalSubcategoryDisease;
		this.doctorDetails = doctorDetails;
		this.image = image;
		this.name = name;
		this.type = type;
	}

	public int getHospitalcategoryid() {
		return hospitalCategoryId;
	}

	public void setHospitalcategoryid(int hospitalcategoryid) {
		this.hospitalCategoryId = hospitalcategoryid;
	}

	public String getHospitalcategoryname() {
		return hospitalCategoryName;
	}

	public void setHospitalcategoryname(String hospitalcategoryname) {
		this.hospitalCategoryName = hospitalcategoryname;
	}

	public String getHospitalcategorypath() {
		return hospitalCategoryPath;
	}

	public void setHospitalcategorypath(String hospitalcategorypath) {
		this.hospitalCategoryPath = hospitalcategorypath;
	}


	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public Set<HospitalSubcategoryDisease> getHospitalSubcategoryDisease() {
		return hospitalSubcategoryDisease;
	}

	public void setHospitalSubcategoryDisease(Set<HospitalSubcategoryDisease> hospitalSubcategoryDisease) {
		this.hospitalSubcategoryDisease = hospitalSubcategoryDisease;
	}



	public Set<DoctorDetails> getDoctorDetails() {
		return doctorDetails;
	}

	public void setDoctorDetails(Set<DoctorDetails> doctorDetails) {
		this.doctorDetails = doctorDetails;
	}
}



