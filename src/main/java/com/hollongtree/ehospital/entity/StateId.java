package com.hollongtree.ehospital.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "state_id")
public class StateId {
	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	@GeneratedValue
	@Column(name = "state_id")
	private int stateId;
	@Column(name = "state_name")
	
	private String stateName;
	@OneToMany(targetEntity = CityList.class,cascade = CascadeType.ALL)
	@JoinColumn(name = "city_fk_id",referencedColumnName = "state_id")
	private List<CityList> city_list;
	
	public StateId() {
		
	}

	public int getStateid() {
		return stateId;
	}

	public void setStateid(int stateid) {
		this.stateId = stateid;
	}

	public String getStatename() {
		return stateName;
	}

	public void setStatename(String statename) {
		this.stateName = statename;
	}

	public List<CityList> getCity_list() {
		return city_list;
	}

	public void setCity_list(List<CityList> city_list) {
		this.city_list = city_list;
	}
	
	

}
