//package com.hollongtree.ehospital.entity;
//
//import java.util.HashSet;
//import java.util.Set;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.ManyToMany;
//import javax.persistence.OneToOne;
//import javax.persistence.Table;
//
//@Entity
//@Table(name="mobile")
//public class MobileNum {
//
//	
//	private int mobileNumid;
//	
//	private Long number;
//	
//	private Set<Registration> registration= new HashSet<Registration>(0);
//
//	
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "Mobile_ID", unique = true, nullable = false)
//	public int getMobileNumid() {
//		return mobileNumid;
//	}
//
//	public void setMobileNumid(int mobileNumid) {
//		this.mobileNumid = mobileNumid;
//	}
//	
//    @Column(name="mobilenum")
//	public Long getNumber() {
//		return number;
//	}
//
//	public void setNumber(Long number) {
//		this.number = number;
//	}
//
//	@ManyToMany(fetch= FetchType.LAZY, mappedBy="mobile")
//	public Set<Registration> getRegistration() {
//		return registration;
//	}
//
//	public void setRegistration(Set<Registration> registration) {
//		this.registration = registration;
//	}
//	
//	
//	
//
//
//}
