package com.hollongtree.ehospital.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;

@Entity
@Table(name = "doctor_details")
public class DoctorDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "doctor_details_id")
	private int doctorDetailsId;
	@Column(name = "doctor_name",length = 100)
	private String doctorName;
	@Column(name = "doctor_address",length = 255)
	private String doctorAddress;
	@Column(name = "hospital_subcategory_id")
	private int hospitalSubcategoryId;
	@Column(name = "active")
	private boolean active;
	@Column(name = "state_id")
	private int stateId;
	@Column(name = "city_id")
	private int cityId;
	@Column(name = "pincode")
	private String pincode;
	@Column(name = "country_id")
	private int countryId;
	@Column(name = "doctor_fee")
	private double doctorFee;

	@Column(name = "udf1",length = 100)
	private String udf1;
	@Column(name = "udf2")
	private String udf2;
	@Column(name = "udf3",length = 1000)
	private String udf3;
	@Column(name = "udf4",length = 1000)
	private String udf4;
	@Column(name = "udf5",length = 1000)
	private String udf5;
	@Column(name = "aggrement_id",length = 100)
	private int aggrementId;
	@Column(name = "image_path",length = 100)
	private String imagePath;
	@Column(name = "charges")
	private double charges;
	@Column(name = "charges_type")
	private String chargesType;
	@Column(name = "no_of_patient")
	private int noOfPatient;
	@Column(name = "from_time")
	private String fromTime;
	@Column(name = "end_time")
	private String endTime;
	@Column(name = "active_time")
	private int activeTime;
	@Column(name = "charges_applicable")
	private boolean chargesApplicable;
	@Column(name = "one_patients_validity")
	private int onePatientsValidity;
	@Column(name = "doctor_email")
	private String doctorEmail;
	@Column(name = "doctor_password")
	private String doctorPassword;

//	@ManyToOne(cascade = CascadeType.MERGE)
//	@JoinColumn(name="hospital_category_id")
//	private HospitalCategory hospitalcategory;


//	@ManyToOne(cascade = CascadeType.ALL,fetch =FetchType.LAZY)
//	private HospitalSubcategoryDisease hospitalSubcategoryDisease;



	@Column(name = "category_image")
	@Lob
	private byte[] image;

	private String name;

	private String type;


	public DoctorDetails() {

	}

	public DoctorDetails(String doctorName, String doctorAddress, int hospitalSubcategoryId, boolean active, int stateId, int cityId, String pincode, int countryId, double doctorFee, String udf1, String udf2, String udf3, String udf4, String udf5, int aggrementId, String imagePath, double charges, String chargesType, int noOfPatient, String fromTime, String endTime, int activeTime, boolean chargesApplicable, int onePatientsValidity, String doctorEmail, String doctorPassword, HospitalSubcategoryDisease hospitalSubcategoryDisease, byte[] image, String name, String type) {
		this.doctorName = doctorName;
		this.doctorAddress = doctorAddress;
		this.hospitalSubcategoryId = hospitalSubcategoryId;
		this.active = active;
		this.stateId = stateId;
		this.cityId = cityId;
		this.pincode = pincode;
		this.countryId = countryId;
		this.doctorFee = doctorFee;
		this.udf1 = udf1;
		this.udf2 = udf2;
		this.udf3 = udf3;
		this.udf4 = udf4;
		this.udf5 = udf5;
		this.aggrementId = aggrementId;
		this.imagePath = imagePath;
		this.charges = charges;
		this.chargesType = chargesType;
		this.noOfPatient = noOfPatient;
		this.fromTime = fromTime;
		this.endTime = endTime;
		this.activeTime = activeTime;
		this.chargesApplicable = chargesApplicable;
		this.onePatientsValidity = onePatientsValidity;
		this.doctorEmail = doctorEmail;
		this.doctorPassword = doctorPassword;
//		this.hospitalSubcategoryDisease = hospitalSubcategoryDisease;
		this.image = image;
		this.name = name;
		this.type = type;
	}

	public int getDoctorDetailsId() {
		return doctorDetailsId;
	}

	public void setDoctorDetailsId(int doctorDetailsId) {
		this.doctorDetailsId = doctorDetailsId;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getDoctorAddress() {
		return doctorAddress;
	}

	public void setDoctorAddress(String doctorAddress) {
		this.doctorAddress = doctorAddress;
	}

	public int getHospitalSubcategoryId() {
		return hospitalSubcategoryId;
	}

	public void setHospitalSubcategoryId(int hospitalSubcategoryId) {
		this.hospitalSubcategoryId = hospitalSubcategoryId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getStateId() {
		return stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public double getDoctorFee() {
		return doctorFee;
	}

	public void setDoctorFee(double doctorFee) {
		this.doctorFee = doctorFee;
	}



	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getUdf4() {
		return udf4;
	}

	public void setUdf4(String udf4) {
		this.udf4 = udf4;
	}

	public String getUdf5() {
		return udf5;
	}

	public void setUdf5(String udf5) {
		this.udf5 = udf5;
	}

	public int getAggrementId() {
		return aggrementId;
	}

	public void setAggrementId(int aggrementId) {
		this.aggrementId = aggrementId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public double getCharges() {
		return charges;
	}

	public void setCharges(double charges) {
		this.charges = charges;
	}

	public String getChargesType() {
		return chargesType;
	}

	public void setChargesType(String chargesType) {
		this.chargesType = chargesType;
	}

	public int getNoOfPatient() {
		return noOfPatient;
	}

	public void setNoOfPatient(int noOfPatient) {
		this.noOfPatient = noOfPatient;
	}

	public String getFromTime() {
		return fromTime;
	}

	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getActiveTime() {
		return activeTime;
	}

	public void setActiveTime(int activeTime) {
		this.activeTime = activeTime;
	}

	public boolean isChargesApplicable() {
		return chargesApplicable;
	}

	public void setChargesApplicable(boolean chargesApplicable) {
		this.chargesApplicable = chargesApplicable;
	}

	public int getOnePatientsValidity() {
		return onePatientsValidity;
	}

	public void setOnePatientsValidity(int onePatientsValidity) {
		this.onePatientsValidity = onePatientsValidity;
	}

	public String getDoctorEmail() {
		return doctorEmail;
	}

	public void setDoctorEmail(String doctorEmail) {
		this.doctorEmail = doctorEmail;
	}

	public String getDoctorPassword() {
		return doctorPassword;
	}

	public void setDoctorPassword(String doctorPassword) {
		this.doctorPassword = doctorPassword;
	}


	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

//	public HospitalSubcategoryDisease getHospitalSubcategoryDisease() {
//		return hospitalSubcategoryDisease;
//	}
//
//	public void setHospitalSubcategoryDisease(HospitalSubcategoryDisease hospitalSubcategoryDisease) {
//		this.hospitalSubcategoryDisease = hospitalSubcategoryDisease;
//	}


	//	public Set<HospitalCategory> getHospitalcategory() {
//		return hospitalcategory;
//	}
//
//	public void setHospitalcategory(Set<HospitalCategory> hospitalcategory) {
//		this.hospitalcategory = hospitalcategory;
//	}

//	public HospitalCategory getHospitalcategory() {
//		return hospitalcategory;
//	}
//
//	public void setHospitalcategory(HospitalCategory hospitalcategory) {
//		this.hospitalcategory = hospitalcategory;
}
