package com.hollongtree.ehospital.entity;

import javax.imageio.ImageIO;
import javax.persistence.*;

import org.apache.tomcat.jni.File;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.awt.image.BufferedImage;
import java.sql.Blob;


@Entity
@Table(name = "lab_test_list")
public class LabTestList {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "lab_test_list_id")
	private int labTestListId;
	@Column(name = "Lab_test_name")
	private String labTestName;
	
	@Column(name = "hospital_id")
	private int hospitalId;
	@Column(name = "test_amount")
	private double testAmount;
	@Column(name = "test_description")
	private String testDescription;
	
	private String name;

	private String type;
	
	@Lob
	private byte[] image;
	
    
	private String udf1;

	private String udf2;

	private String udf3;

	private String udf4;

	private String udf5;
	
	
	
	public LabTestList() {
		// TODO Auto-generated constructor stub
	}
	
	


    @JsonCreator
	public LabTestList(int labTestListId, String labTestName, int hospitalId, double testAmount,
			String testDescription, String name, String type, byte[] image, String udf1, String udf2, String udf3,
			String udf4, String udf5) {
		super();
		this.labTestListId = labTestListId;
		this.labTestName = labTestName;
		this.hospitalId = hospitalId;
		this.testAmount = testAmount;
		this.testDescription = testDescription;
		this.name = name;
		this.type = type;
		this.image = image;
		this.udf1 = udf1;
		this.udf2 = udf2;
		this.udf3 = udf3;
		this.udf4 = udf4;
		this.udf5 = udf5;
		
		
		
	}





	public int getLabTestListId() {
		return labTestListId;
	}



	public void setLabTestListId(int labTestListId) {
		this.labTestListId = labTestListId;
	}



	public String getLabTestName() {
		return labTestName;
	}



	public void setLabTestName(String labTestName) {
		this.labTestName = labTestName;
	}



	public int getHospitalId() {
		return hospitalId;
	}



	public void setHospitalId(int hospitalId) {
		this.hospitalId = hospitalId;
	}



	public double getTestAmount() {
		return testAmount;
	}



	public void setTestAmount(double testAmount) {
		this.testAmount = testAmount;
	}



	public String getTestDescription() {
		return testDescription;
	}



	public void setTestDescription(String testDescription) {
		this.testDescription = testDescription;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}



	public byte[] getImage() {
		return image;
	}



	public void setImage(byte[] image) {
		this.image = image;
	}



	public String getUdf1() {
		return udf1;
	}



	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}



	public String getUdf2() {
		return udf2;
	}



	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}



	public String getUdf3() {
		return udf3;
	}



	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}



	public String getUdf4() {
		return udf4;
	}



	public void setUdf4(String udf4) {
		this.udf4 = udf4;
	}



	public String getUdf5() {
		return udf5;
	}



	public void setUdf5(String udf5) {
		this.udf5 = udf5;
	}
}