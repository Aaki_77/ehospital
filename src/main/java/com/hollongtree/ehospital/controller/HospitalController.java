package com.hollongtree.ehospital.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.Hospital;
import com.hollongtree.ehospital.entity.HospitalCatalogue;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.HospitalService;


@RestController
@Validated
public class HospitalController  {
	
	@Autowired
	HospitalService hospitalservice;
	
	/**API for adding hospital entity passing the data to the service class
	 * 
	 * @param hospital
	 * @return ResponseEntity and httpstatus
	 */
    @PostMapping(value = "/hospital")
	public ResponseEntity<Hospital> addEntryHospital(@RequestBody  Hospital hospital)
	{
		Hospital hs=null;
		
		hs=hospitalservice.saveHospital(hospital);
		
		return ResponseEntity.status(HttpStatus.OK).body(hs);
	}
    /**API for fetch Hospital data by hospital name
     * passing the value to the service class
     * @param hospitalName
     * @return ResponseEntity
     * @throws EhospitalException
     */
	@GetMapping(path="hospital/{hospitalName}")
	public ResponseEntity<Hospital> getHospital(@PathVariable("hospitalName") String hospitalName) throws EhospitalException  
	{
		Hospital h = hospitalservice.getHospital(hospitalName);
			
		return ResponseEntity.status(HttpStatus.OK).body(h);
		
	}
	/**API for Update Hospital data by in hospital entity
     * passing the value to the service class
     * @param hospital
     * @return ResponseEntity
     * @throws EhospitalException
     */
	@PutMapping(path="hospitalUpdate/{hospitalId}")
	public ResponseEntity<Hospital> updateHospital(@RequestBody Hospital hospital,@PathVariable("hospitalId")Integer hospitalId) throws EhospitalException
	{
		Hospital h= hospitalservice.updateHospital(hospital,hospitalId);
		return ResponseEntity.status(HttpStatus.OK).body(h);
	}
	
	

	

}
