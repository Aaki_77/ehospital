package com.hollongtree.ehospital.controller;


import com.hollongtree.ehospital.EhospitalApplication;
import com.hollongtree.ehospital.entity.Patient;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.PatientService;

import java.util.List;
import java.util.logging.LogManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("http://localhost:8100")
public class PatientController {

    private static final Logger logger = LoggerFactory.getLogger(EhospitalApplication.class);

    @Autowired
    PatientService patientService;

    @GetMapping("/test")
    public String test(Patient patient){

       return "PATIENT";
    }



    @PostMapping("/savePatient")
    @ResponseBody
    public ResponseEntity<Patient> savePatient(@RequestBody Patient patient) throws EhospitalException {
        logger.info(" Api for Post patient details  by using Requestbody (/savePatient)");


        Patient pt = patientService.savePatient(patient);

        if (null != patient.getPatientid()) {

            return new ResponseEntity<Patient>(pt, HttpStatus.CREATED);

        }
        logger.info(" Warn :  Bad request  ");
        return new ResponseEntity<Patient>(HttpStatus.BAD_REQUEST);

    }

    
	@GetMapping(path = "/patient/{patientid}")
	public ResponseEntity<Patient> getPatient(@PathVariable("patientid")String patientid) throws EhospitalException
	{
		Patient patient=patientService.getPatient(patientid);
		
		return ResponseEntity.status(HttpStatus.OK).body(patient);
	}
	
	@GetMapping(path = "/patientDetails")
	Page<Patient> patientPageble(Pageable pageable)
	{
		Page<Patient> page=patientService.getpatientPageble(pageable);
		return page;
	}
	
	@PostMapping(path = "log/{email}/{password}")
	public ResponseEntity<Patient> addLogin(@PathVariable("email") String email,
			@PathVariable("password") String password) {
		
		Patient log;
		try {
			log = patientService.addPatientLogin(email, password);
			return ResponseEntity.status(HttpStatus.OK).body(log);
		} catch (EhospitalException e) {
			log = null;
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(log);
		}
		
	}
	
	@GetMapping(path = "login/get")
	public ResponseEntity<List<Patient>> greet() {
		List<Patient> log = patientService.getPatient();
		return ResponseEntity.status(HttpStatus.OK).body(log);

	}
	


}

