//package com.hollongtree.ehospital.controller;
//
//
//import com.hollongtree.ehospital.entity.LoginHistory;
//import com.hollongtree.ehospital.service.LoginService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//public class LoginController {
//
//    @Autowired
//    LoginService loginService;
//
//    @GetMapping ("/login")
//    public String test3(LoginHistory login){
//
//        return "login";
//}
//
//    @GetMapping ("/loginHistory/{patientId}")
//    public ResponseEntity<LoginHistory> getLoginHistoryForPatient(@PathVariable String patientId){
//
//       LoginHistory loginHistory= loginService.getLoginHistoryForPatient(patientId);
//
//            if(null != loginHistory){
//
//               return new ResponseEntity<LoginHistory>(loginHistory, HttpStatus.OK);
//            }
//          return new ResponseEntity<LoginHistory>(HttpStatus.BAD_REQUEST);
//
//        }
//
//    @GetMapping ("/loginHistory")
//    public ResponseEntity<List<LoginHistory>> getLoginHistoryForAllPatient(){
//
//       List<LoginHistory> loginHistoryList= loginService.getLoginHistoryForALLPatient();
//
//        if(loginHistoryList.size()>0){
//
//            return new ResponseEntity<List<LoginHistory>>(loginHistoryList, HttpStatus.OK);
//        }
//        return new ResponseEntity<List<LoginHistory>>(HttpStatus.BAD_REQUEST);
//
//    }
//
//
//}
