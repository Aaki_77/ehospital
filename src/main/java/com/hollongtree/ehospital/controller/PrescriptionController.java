package com.hollongtree.ehospital.controller;


import com.hollongtree.ehospital.entity.Patient;
import com.hollongtree.ehospital.entity.PrescriptionList;
import com.hollongtree.ehospital.service.PrescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PrescriptionController {

    @Autowired
    PrescriptionService prescriptionService;

    @GetMapping("/Prescreption")
    public String test2(PrescriptionList prescription) {

        return "prescription";
    }


    @PostMapping("/savePrescription")
    public ResponseEntity<String> savePrescription(@RequestBody PrescriptionList prescription) {


        prescriptionService.savePrescription(prescription);

            if(null != prescription.getPrescriptionid()){

                return new ResponseEntity<String>(prescription.getPrescriptionid(), HttpStatus.OK);
            }
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);

        }
}