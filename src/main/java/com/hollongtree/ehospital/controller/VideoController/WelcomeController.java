package com.hollongtree.ehospital.controller.VideoController;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import com.hollongtree.ehospital.entity.LoginBean;

@Controller
public class WelcomeController {

	// inject via application.properties
	@Value("${welcome.message:test}")
	private String message = "Hello!!!!";

	@RequestMapping("/hello")
	public String welcome(Map<String, Object> model) {
		model.put("message", this.message);
		return "welcome";
	}
	
	@RequestMapping(value = "login")
    public String init(Model model) {
        model.addAttribute("msg", "Please Enter Your Login Details");
        return "login";
    }
	
	@PostMapping("/login")
	    public String submit(Model model,
	            @ModelAttribute("loginBean") LoginBean loginBean) {
	        if (loginBean != null && loginBean.getUserName() != null
	                & loginBean.getPassword() != null) {
	            if (loginBean.getUserName().equals("Aakash")
	                    && loginBean.getPassword().equals("Aakash123")) {
	                model.addAttribute("msg", loginBean.getUserName());
	                return "success";
	            } 
	            else {
	                model.addAttribute("error", "Invalid Details");
	                return "login";
	            }
	        } else {
	            model.addAttribute("error", "Please enter Details");
	            return "login";
	        }
	    }

}