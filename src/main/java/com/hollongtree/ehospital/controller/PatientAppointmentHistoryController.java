package com.hollongtree.ehospital.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.hollongtree.ehospital.entity.PatientAppointmentHistory;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.PatientAppointmentHistoryService;

@RestController
@CrossOrigin("http://localhost:8100")
public class PatientAppointmentHistoryController {

	@Autowired
	PatientAppointmentHistoryService patientAppointmentHistoryService;

	/**
	 * API for save the data to the patientAppointmentHistory, passing the value to the service class
	 * @param patientAppointmentHistory
	 * @return
	 */
	@PostMapping(path = "patientAppointmentHistory")
	public ResponseEntity<PatientAppointmentHistory> addEntryPatientAppointmentHistory(@RequestBody PatientAppointmentHistory patientAppointmentHistory) throws InterruptedException
	{
		PatientAppointmentHistory pah=patientAppointmentHistoryService.addPatientAppointmentHistory(patientAppointmentHistory);
		return ResponseEntity.status(HttpStatus.OK).body(pah);

	}

	/**
	 * API for Updating the patientAppointmentHistory entity
	 * @param patientAppointmentHistory
	 * @return
	 * @throws EhospitalException
	 */
	@PutMapping(path = "patientAppointmentHistoryUpdate/{patientAppointmentHistoryId}")
	public ResponseEntity<PatientAppointmentHistory> saveOrUpdatePatientAppointmentHistory(@RequestBody PatientAppointmentHistory patientAppointmentHistory,int patientAppointmentHistoryId) throws EhospitalException,InterruptedException
	{
		PatientAppointmentHistory pah=patientAppointmentHistoryService.updatePatientAppointmentHistory(patientAppointmentHistory,patientAppointmentHistoryId);
		return ResponseEntity.status(HttpStatus.OK).body(pah);

	}

	@GetMapping(path = "patientAppointmentHistory/{doctorDetailsId}")
	public ResponseEntity<PatientAppointmentHistory> getPatientAppointmentHistory(@RequestBody PatientAppointmentHistory patientAppointmentHistory,@PathVariable("doctorDetailsId")int doctorDetailsId) throws EhospitalException
	{
		PatientAppointmentHistory pah=patientAppointmentHistoryService.getPatientAppointmentHistory(doctorDetailsId);
		return ResponseEntity.status(HttpStatus.OK).body(pah);

	}

	@GetMapping(path = "doctorDetailsByPatientId/{patientId}")
	public ResponseEntity<PatientAppointmentHistory> getdoctorDetailsByPatientId(@PathVariable("patientId")String patientId) throws EhospitalException
	{
		PatientAppointmentHistory pah=patientAppointmentHistoryService.getdoctorDetailsByPatientId(patientId);
		return ResponseEntity.status(HttpStatus.OK).body(pah);

	}

	@GetMapping(path = "PatientAppointmentHistoryByPatientId/{patientId}")
	public ResponseEntity<List<PatientAppointmentHistory>> getPatientAppointmentHistoryByPatientId(@PathVariable("patientId")String patientId) throws EhospitalException
	{
		List<PatientAppointmentHistory> pah=patientAppointmentHistoryService.getPatientAppointmentHistoryByPatientId(patientId);
		return ResponseEntity.status(HttpStatus.OK).body(pah);

	}

	@GetMapping(path = "getPatientAppointmentHistory")
	public ResponseEntity<List<PatientAppointmentHistory>> getPatientAppointmentHistory() throws EhospitalException
	{
		List<PatientAppointmentHistory> pah=patientAppointmentHistoryService.getPatientAppointmentHistory();
		return ResponseEntity.status(HttpStatus.OK).body(pah);

	}



	@GetMapping(path = "getPatientAppointmentHistoryByDate")
	public ResponseEntity<List<PatientAppointmentHistory>> getPatientAppointmentHistoryByDate() throws EhospitalException
	{
		List<PatientAppointmentHistory> pah=patientAppointmentHistoryService.getPatientAppointmentHistoryByDate();
		return ResponseEntity.status(HttpStatus.OK).body(pah);

	}

	@GetMapping ("/getPatientAppointmentHistory/{patientAppointmentHistoryId}")
	public ResponseEntity<PatientAppointmentHistory> getpatientappointmenthistory(@PathVariable Integer patientAppointmentHistoryId){

		PatientAppointmentHistory testReport= patientAppointmentHistoryService.getappointmenthistoryForPatient(patientAppointmentHistoryId);

		if(testReport != null){

			return new ResponseEntity<PatientAppointmentHistory>(testReport, HttpStatus.OK);
		}
		return new ResponseEntity<PatientAppointmentHistory>(HttpStatus.BAD_REQUEST);

	}


}
