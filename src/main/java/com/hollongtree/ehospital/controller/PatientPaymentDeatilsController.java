package com.hollongtree.ehospital.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.PatientPaymentDetails;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.PatientPaymentDeatilsService;

@RestController
@CrossOrigin("http://localhost:8100")
public class PatientPaymentDeatilsController {

	@Autowired
	PatientPaymentDeatilsService patientPaymentDeatilsService;

	/**
	 * API for save the patientPaymentDeatils, for that passing the value to the
	 * service class
	 * 
	 * @param patientPaymentDeatils
	 * @return
	 */
	@PostMapping(path = "patientPaymentDetails")
	public ResponseEntity<PatientPaymentDetails> addEntryPatientPaymentDeatils(
			 PatientPaymentDetails patientPaymentDeatils) {
		PatientPaymentDetails ppd = patientPaymentDeatilsService.addPatientPaymentDetails(patientPaymentDeatils);
		return ResponseEntity.status(HttpStatus.OK).body(ppd);

	}

	/**
	 * API for fetch the data from the patientPaymentDeatils entity using
	 * patientsname
	 * 
	 * @param patientsName
	 * @return
	 * @throws EhospitalException
	 */
	@GetMapping("patientPaymentDeatils/patientsName")
	public ResponseEntity<PatientPaymentDetails> getPatientPaymentDetails(
			@PathVariable("patientsName") String patientsName) throws EhospitalException {
		PatientPaymentDetails ppd = patientPaymentDeatilsService.getPatientPaymentDetails(patientsName);

		return ResponseEntity.status(HttpStatus.OK).body(ppd);
	}

	/**
	 * API for Update patientPaymentDeatils entity
	 * 
	 * @param patientPaymentDeatils
	 * @return
	 * @throws EhospitalException 
	 */
	@PutMapping(path = "patientPaymentDeatilsUpdate/{patientsAppointmentDetailsId}")
	public ResponseEntity<PatientPaymentDetails> saveOrUspdatePatientPaymentDeatils(
			@RequestBody PatientPaymentDetails patientPaymentDeatils,@PathVariable("patientsAppointmentDetailsId")int patientsAppointmentDetailsId) throws EhospitalException {
		PatientPaymentDetails ppd = patientPaymentDeatilsService.updatePatientPaymentDetails(patientPaymentDeatils,patientsAppointmentDetailsId);
		return ResponseEntity.status(HttpStatus.OK).body(ppd);

	}
	
	@GetMapping("patientPaymentDeatils/{patientsId}")
	public ResponseEntity<PatientPaymentDetails> getPatientPaymentDetailsByPatientsId(
			@PathVariable("patientsId") int patientsId) throws EhospitalException {
		PatientPaymentDetails ppd = patientPaymentDeatilsService.getPatientPaymentDetailsByPatientsId(patientsId);

		return ResponseEntity.status(HttpStatus.OK).body(ppd);
	}
	
	@GetMapping("getPatientPaymentDeatils")
	public ResponseEntity<List<PatientPaymentDetails>> getPatientPaymentDetails() throws EhospitalException {
		List<PatientPaymentDetails> ppd = patientPaymentDeatilsService.getPatientPaymentDetails();

		return ResponseEntity.status(HttpStatus.OK).body(ppd);
	}

}
