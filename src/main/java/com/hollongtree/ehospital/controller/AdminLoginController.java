package com.hollongtree.ehospital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.AdminLogin;
import com.hollongtree.ehospital.entity.AuthRequest;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.AdminLoginService;

@RestController
public class AdminLoginController {
	
//	@Autowired
//    private JwtUtil jwtUtil;
//    @Autowired
//    private AuthenticationManager authenticationManager;
	@Autowired
	private AdminLoginService logservice;
	
	@PostMapping(path = "login/{email}/{password}")
	public ResponseEntity<AdminLogin> addLogin(@PathVariable("email") String email,@PathVariable("password")String password) throws EhospitalException
	{
		AdminLogin log=logservice.addAdminLogin(email,password);
		return ResponseEntity.status(HttpStatus.OK).body(log);
	}
	
	@PutMapping(path = "login/{loginId}")
	public ResponseEntity<AdminLogin> updateLogin(@RequestBody AdminLogin login, @PathVariable("loginId") int loginId) throws EhospitalException
	{
		AdminLogin log=logservice.updateAdminLogin(login, loginId);
		return ResponseEntity.status(HttpStatus.OK).body(log);
		
	}
	
	@GetMapping(path = "login/greet")
	public ResponseEntity<String> greet()
	{
		return ResponseEntity.status(HttpStatus.OK).body("hi");
		
	}
	
	

//    @PostMapping("authenticate")
//    public String generateToken(AuthRequest authRequest) throws Exception {
//        try {
//            authenticationManager.authenticate(
//                    new UsernamePasswordAuthenticationToken(authRequest.getEmail(), authRequest.getPassword())
//            );
//        } catch (Exception ex) {
//            throw new Exception("inavalid username/password");
//        }
//        return jwtUtil.generateToken(authRequest.getEmail());
//    }
	
	

}
