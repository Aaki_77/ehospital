package com.hollongtree.ehospital.controller;

import com.hollongtree.ehospital.entity.LabTestList;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.LabtestListService;
import com.hollongtree.ehospital.controller.ResponseFile;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@CrossOrigin("http://localhost:8100")
public class LabtestListController {

    @Autowired
    LabtestListService labtestService;

    @GetMapping("/labtest")
    public String test6(LabTestList labTestList) {

        return "labtest";
    }

    @PostMapping("/saveLabtest")
    public ResponseEntity<LabTestList> saveLocation(@RequestBody LabTestList labTestList) throws IOException{

       LabTestList lb= labtestService.saveLabtest(labTestList);
       
       return ResponseEntity.status(HttpStatus.OK).body(lb);
    }
    
//    @GetMapping("labList/{labTestListId}")
//    public ResponseEntity<Stream<LabTestList>> getLabtestlist(@PathVariable("labTestListId")int labTestListId) throws EhospitalException
//	{
//    	System.out.println(labTestListId);
//    	Stream<LabTestList> ltl = labtestService.getLabTest(labTestListId);
//		return ResponseEntity.status(HttpStatus.OK).body(ltl);
//	}
//    
    @GetMapping("labList/{hospitalId}")
    public ResponseEntity<Stream<LabTestList>> getLabtestlistBy(@PathVariable("hospitalId")int hospitalId) throws EhospitalException
	{
    	Stream<LabTestList> ltl = labtestService.getLabTestBy(hospitalId);
		return ResponseEntity.status(HttpStatus.OK).body(ltl);
	}
    
    @GetMapping("labList")
    public ResponseEntity<List<LabTestList>> getAllLabtestlist() throws EhospitalException, SQLException
	{
		List<LabTestList> ltl = labtestService.getAllLabTest();
		return ResponseEntity.status(HttpStatus.OK).body(ltl);
		
	}
    
    @PutMapping("labListUpdate/{labTestListId}")
    public ResponseEntity<LabTestList> updateLabtestlist(@PathVariable("labTestListId") int labTestListId,@PathVariable("file") MultipartFile file) throws EhospitalException, IOException, SerialException, SQLException
	{
		LabTestList ltl = labtestService.updateLabTest(file,labTestListId);
		
		return ResponseEntity.status(HttpStatus.OK).body(ltl);
	}
}


//@GetMapping("/files")
//public ResponseEntity<List<ResponseFile>> getListFiles() {
//	List<ResponseFile> files = labtestService.getAllFiles().map(dbFile -> {
//		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/files/")
//				.path("hi").toUriString();
//		
//		return new ResponseFile(dbFile.getName(), fileDownloadUri, dbFile.getType(), dbFile.getData().length);
//	}).collect(Collectors.toList());
//	return ResponseEntity.status(HttpStatus.OK).body(files);
//}
//}




		
//    @GetMapping("/files")
//    public ResponseEntity<List<ResponseFile>> getListFiles() {
//    	labtestService.getAllFiles().map(dbFile -> {
//    		UriComponentsBuilder fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/files/");
//    	
//		return null;
//    	}
//    }
//}
    	


//			List<ResponseFile> files = storageService.getAllFiles().map(dbFile -> {
//				String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/files/")
//						.path(dbFile.getId()).toUriString();
//
//				return new ResponseFile(dbFile.getName(), fileDownloadUri, dbFile.getType(), dbFile.getData().length);
//			}).collect(Collectors.toList());
//			return ResponseEntity.status(HttpStatus.OK).body(files);