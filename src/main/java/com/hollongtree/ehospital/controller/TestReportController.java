package com.hollongtree.ehospital.controller;

import com.hollongtree.ehospital.entity.LabTestList;
//import com.hollongtree.ehospital.entity.Hospital;
import com.hollongtree.ehospital.entity.TestReport;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.TestReportService;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("http://localhost:8100")
public class TestReportController {

    @Autowired
    TestReportService testReportService;

    @GetMapping("/testReport")
    public String test7(TestReport testReport){

        return "testReport";
    }

    @PostMapping("/savetestReport")
    public ResponseEntity<Integer> saveTestReport(@RequestBody TestReport testReport){

        testReportService.saveTestReport(testReport);

            if(null != testReport.getTestReportid()){

                return new ResponseEntity<Integer>(testReport.getTestReportid(), HttpStatus.OK);
            }
            return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);


        }
    @GetMapping("getTest")
    public ResponseEntity<List<TestReport>> getAllLabtestlist() throws EhospitalException
	{
		List<TestReport> ltl = testReportService.getAllTest();
		
		return ResponseEntity.status(HttpStatus.OK).body(ltl);
	}
    
}