package com.hollongtree.ehospital.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hollongtree.ehospital.entity.MedicineList;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.MedicineListService;

@RestController
@CrossOrigin("http://localhost:8100")
public class MedicineListController {

	@Autowired
	MedicineListService medicineListService;

	/**
	 * API for adding the data to the db, passing the values to the service class
	 * 
	 * @param medicineList
	 * @return
	 */
	@PostMapping(path = "medicineList")
	public ResponseEntity<MedicineList> addEntryMedicineList( MedicineList medicineList) {
		MedicineList ml = medicineListService.addMedicineList(medicineList);
		return ResponseEntity.status(HttpStatus.OK).body(ml);
	}

	/**
	 * API for fetching the data from the db using medicinename if not found then
	 * throw a exception
	 * 
	 * @param medicineName
	 * @return
	 * @throws EhospitalException
	 */
	@GetMapping(path = "medicineList/{medicineName}")
	public ResponseEntity<MedicineList> getMedicineList(@PathVariable("medicineName") String medicineName)
			throws EhospitalException {
		MedicineList ml = medicineListService.getMedicineList(medicineName);

		return ResponseEntity.status(HttpStatus.OK).body(ml);
	}

	/**
	 * API for updating the medicalList entity, passing the value to the service
	 * class
	 * 
	 * @param medicineList
	 * @return
	 * @throws EhospitalException 
	 */
	@PutMapping(path = "medicineListUpdate/{medicineId}")
	public ResponseEntity<MedicineList> updateMedicineList(MedicineList medicineList,@PathVariable("medicineId") int medicineId) throws EhospitalException {
		MedicineList ml = medicineListService.updateMedicineList(medicineList,medicineId);
		return ResponseEntity.status(HttpStatus.OK).body(ml);
	}
	
	@PutMapping(path = "medicineList/{medicineId}")
	public ResponseEntity<MedicineList> updateMedicineList(@PathVariable("file") MultipartFile file,@PathVariable("medicineId") int medicineId) throws EhospitalException, IOException {
		MedicineList ml = medicineListService.updateMedicineListImage(file,medicineId);
		return ResponseEntity.status(HttpStatus.OK).body(ml);
	}

	@GetMapping(path = "medicineList")
	public ResponseEntity<List<MedicineList>> getMedicineListAll()
			throws EhospitalException {
		List<MedicineList> ml = medicineListService.getMedicineListAll();

		return ResponseEntity.status(HttpStatus.OK).body(ml);
	}
	
	@GetMapping(path = "medicineList/details/{medicineId}")
	public ResponseEntity<List<MedicineList>> getMedicineList(@PathVariable("medicineId")int medicineId)
			throws EhospitalException {
		List<MedicineList> ml = medicineListService.getMedicineList(medicineId);

		return ResponseEntity.status(HttpStatus.OK).body(ml);
	}
}
