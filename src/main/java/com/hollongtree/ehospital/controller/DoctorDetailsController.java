package com.hollongtree.ehospital.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.hollongtree.ehospital.entity.DoctorDetails;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.DoctorDetailsService;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.rowset.serial.SerialException;

@RestController
@CrossOrigin("http://localhost:8100")

public class DoctorDetailsController {

	@Autowired
	DoctorDetailsService doctorDetailsService;

	/**
	 * API for passing the data to the service class
	 *
	 * @param doctordetails
	 * @return
	 */
	@PostMapping(path = "doctorDetails")
	public ResponseEntity<DoctorDetails> addEntryDoctorDetails(@RequestBody DoctorDetails doctordetails) {
		DoctorDetails dd = doctorDetailsService.addDoctorDetails(doctordetails);
		return ResponseEntity.status(HttpStatus.OK).body(dd);

	}

	/**
	 * API for fetching the data from doctor_details entity, passing the data to the
	 * service class
	 *
	 * @param doctorname
	 * @return
	 * @throws EhospitalException
	 */
	@GetMapping(path = "doctorDetails/{doctorName}")
	public ResponseEntity<DoctorDetails> getDoctorDetailsByDoctorName(@PathVariable("doctorName") String doctorName)
			throws EhospitalException {
		DoctorDetails dd = doctorDetailsService.getDoctorDetailsByDoctorName(doctorName);
		return ResponseEntity.status(HttpStatus.OK).body(dd);

	}

	@GetMapping(path = "doctorDetailsByPatientId/{patient}")
	public ResponseEntity<DoctorDetails> getDoctorDetailsByPatientId(@PathVariable("patient") String patientid)
			throws EhospitalException {
		DoctorDetails dd = doctorDetailsService.getDoctorDetailsByPatientId(patientid);
		return ResponseEntity.status(HttpStatus.OK).body(dd);

	}


	@GetMapping(path = "doctorDetails")
	public ResponseEntity<List<DoctorDetails>> getDoctorDetails()
			throws EhospitalException {
		List<DoctorDetails> dd = doctorDetailsService.getDoctorDetails();
		return ResponseEntity.status(HttpStatus.OK).body(dd);

	}

	/**
	 * API for updating the record in the doctor_details entity
	 *
	 * @param doctordetails
	 * @return
	 * @throws EhospitalException
	 */
//	@PutMapping(path = "doctorDetailsUpdate/{doctorDetailsId}")
//	public ResponseEntity<DoctorDetails> saveOrUpdateDoctorDetails(@RequestBody DoctorDetails doctordetails,@PathVariable("doctorDetailsId")int doctorDetailsId) throws EhospitalException {
//		DoctorDetails dd = doctorDetailsService.updateDoctorDetails(doctordetails,doctorDetailsId);
//		return ResponseEntity.status(HttpStatus.OK).body(dd);
//
//	}

	@PutMapping("doctorDetailsUpdate/{doctorDetailsId}")
	public ResponseEntity<DoctorDetails> doctorDetails(@PathVariable("doctorDetailsId") int doctorDetailsId,@PathVariable("file") MultipartFile file) throws EhospitalException, IOException, SerialException
			, SQLException
	{
		DoctorDetails ltl = doctorDetailsService.doctorDetails(file,doctorDetailsId);

		return ResponseEntity.status(HttpStatus.OK).body(ltl);
	}


	@GetMapping("docthossubcat/{hospitalSubcategoryId}")
	public ResponseEntity<List<DoctorDetails>> getDoctorDetailsBysubcatId(@PathVariable("hospitalSubcategoryId") int hospitalSubcategoryId)
			throws EhospitalException {


		List<DoctorDetails> dd1 = doctorDetailsService.getDoctorDetailsBysubcatId(hospitalSubcategoryId);
		return ResponseEntity.status(HttpStatus.OK).body(dd1);


	}


	@GetMapping("doct2/{hospitalCategoryId}/{hospitalSubcategoryId}")
	public ResponseEntity<List<DoctorDetails>> getDoctorDetailsBysubcatId2(@PathVariable("hospitalSubcategoryId") int hospitalSubcategoryId,@PathVariable ("hospitalCategoryId") int hospitalCategoryId)
			throws EhospitalException {


		List<DoctorDetails> dd2 = doctorDetailsService.getDoctorDetailsBysubcatId(hospitalSubcategoryId);
		return ResponseEntity.status(HttpStatus.OK).body(dd2);


	}







	@GetMapping("/getTes")
	public ResponseEntity<List<DoctorDetails>> getAlllist() throws EhospitalException
	{
		List<DoctorDetails> ltl = doctorDetailsService.getAlllist();

		return ResponseEntity.status(HttpStatus.OK).body(ltl);
	}


//	@GetMapping ("/DoctorDetails/{doctorDetailsId}")
//	public ResponseEntity<DoctorDetails> getDoctorForPatient(@PathVariable Integer doctorDetailsId){
//
//		DoctorDetails DD= doctorDetailsService.getDoctorForPatient(doctorDetailsId);
//
//		if(DD != null){
//
//			return new ResponseEntity<DoctorDetails>(DD, HttpStatus.OK);
//		}
//		return new ResponseEntity<DoctorDetails>(HttpStatus.BAD_REQUEST);
//
//	}

	@GetMapping(path = "ddd/{doctorDetailsId}")
	public ResponseEntity<List<DoctorDetails>> getDoctorDetailsById(@PathVariable("doctorDetailsId") Integer doctorDetailsId)
			throws EhospitalException {
		List<DoctorDetails> DDD = doctorDetailsService.getDoctorDetailsById(doctorDetailsId);
		return ResponseEntity.status(HttpStatus.OK).body(DDD);

	}

	@GetMapping("docthossubcat11/{hospitalSubcategoryId}")
	public ResponseEntity<List<DoctorDetails>> getDoctorDetailsBysubcatId11(@PathVariable("hospitalSubcategoryId") int hospitalSubcategoryId)
			throws EhospitalException {


		List<DoctorDetails> dd11 = doctorDetailsService.getDoctorDetailsBysubcatId(hospitalSubcategoryId);
		return ResponseEntity.status(HttpStatus.OK).body(dd11);


	}


}
