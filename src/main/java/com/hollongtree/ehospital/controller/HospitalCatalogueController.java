package com.hollongtree.ehospital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.hollongtree.ehospital.entity.HospitalCatalogue;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.HospitalCatalogueService;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:8100")

public class HospitalCatalogueController {

	@Autowired
	HospitalCatalogueService hospitalCatalogueService;

	/**
	 * Api for adding the values of hospitalcatalogue to the db for that passing the
	 * param to the service class
	 * 
	 * @param hospitalcatalogue
	 * @return
	 */
	@PostMapping(path = "hospitalCatalogue")
	public ResponseEntity<HospitalCatalogue> addEntryHospitalCatalogue(@RequestBody HospitalCatalogue hospitalcatalogue) {
		HospitalCatalogue hc = hospitalCatalogueService.addHospitalCatalogue(hospitalcatalogue);
		return ResponseEntity.status(HttpStatus.OK).body(hc);
	}

	/**
	 * Api for fetching the data using catalogue name and handling exception if
	 * there is any null value
	 * 
	 * @param catalogueName
	 * @return response entity and httpstatus
	 */
	@GetMapping("hospitalCatalogue/{catalogueName}")
	public ResponseEntity<HospitalCatalogue> getHospitalCatalogue(@PathVariable("catalogueName") String catalogueName) {
		HospitalCatalogue hc = null;
		try {
			hc = hospitalCatalogueService.getHospitalCatalogue(catalogueName);
		} catch (EhospitalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.status(HttpStatus.OK).body(hc);
	}

	/**
	 * Update hospitalcatalogue passing the value to the service class
	 * 
	 * @param hospitalCatalogue
	 * @return reponse entity and http status
	 * @throws EhospitalException 
	 */
	@PutMapping(path = "hospitalCatalogueupdate/{hospitalCatalogueId}")
	public ResponseEntity<HospitalCatalogue> saveOrupdateHospitalCatalogue(
			@RequestBody HospitalCatalogue hospitalCatalogue,@PathVariable("hospitalCatalogueId")int hospitalCatalogueId) throws EhospitalException {
		HospitalCatalogue hcat = hospitalCatalogueService.updateHospitalCatalogue(hospitalCatalogue,hospitalCatalogueId);
		return ResponseEntity.status(HttpStatus.OK).body(hcat);
	}


	@GetMapping("/gethospitalCatalogue")
	public ResponseEntity<List<HospitalCatalogue>> getAllLabtestlist() throws EhospitalException
	{
		List<HospitalCatalogue> ltl = hospitalCatalogueService.getAllHC();

		return ResponseEntity.status(HttpStatus.OK).body(ltl);
	}
}
