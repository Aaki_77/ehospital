package com.hollongtree.ehospital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.MedicineImageList;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.MedicineImageListService;

@RestController
public class MedicineImageListController {
	
	@Autowired
	MedicineImageListService medicineImageListService;
	
	/**
	 * API for adding data to the MedicineImageList entity, passing the data to the service class
	 * @param medicineImageList
	 * @return
	 */
	@PostMapping(path = "medicineImageList")
	public ResponseEntity<MedicineImageList> addEntryMedicineImageList(@RequestBody MedicineImageList medicineImageList)
	{
		MedicineImageList mil=medicineImageListService.addMedicineImageList(medicineImageList);
		return ResponseEntity.status(HttpStatus.OK).body(mil);
	}
	
	/**
	 * API for Update the MedicineImageList entity
	 * @param medicineImageList
	 * @return
	 * @throws EhospitalException 
	 */
	@PutMapping(path = "medicineImageListUpdate")
	public ResponseEntity<MedicineImageList> updateMedicineImageList(@RequestBody MedicineImageList medicineImageList,@PathVariable("medicineImageId")int medicineImageId) throws EhospitalException
	{
		MedicineImageList mil=medicineImageListService.updateMedicineImageList(medicineImageList,medicineImageId);
		return ResponseEntity.status(HttpStatus.OK).body(mil);
	}

}
