package com.hollongtree.ehospital.controller;

import com.hollongtree.ehospital.Repository.HospitalSubcategoryDiseaseDao;
import com.hollongtree.ehospital.entity.HospitalCategory;
import com.hollongtree.ehospital.exception.HospitalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.hollongtree.ehospital.entity.HospitalSubcategoryDisease;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.HospitalSubcategoryDiseaseService;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.rowset.serial.SerialException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@RestController
@CrossOrigin("http://localhost:8100")

public class HospitalSubcategoryDiseaseController {

	@Autowired
	HospitalSubcategoryDiseaseService hospitalSubcategoryDiseaseService;

	@Autowired
	HospitalSubcategoryDiseaseDao hospitalSubcategoryDiseaseDao;

	/**Api for adding the values of hospitalSubcategoryDisease to the db
	 * for that passing the param to the service class
	 * @param hospitalcatalogues
	 * @return responseentity and httpstatus of the code
	 */
	@PostMapping(path = "hospitalSubcategoryDisease")
	public ResponseEntity<HospitalSubcategoryDisease> addEntryHospitalSubcategoryDisease(
			@RequestBody HospitalSubcategoryDisease hospitalSubcategoryDisease)
	{
		HospitalSubcategoryDisease hsd = hospitalSubcategoryDiseaseService
				.addHospitalSubcategoryDisease(hospitalSubcategoryDisease);

		return ResponseEntity.status(HttpStatus.OK).body(hsd);
	}
	/** Api for fetching the data using hospitalSubcategoryName
	 * and handling the exception if there is any null value
	 * @param hospitalCategoryName
	 * @return response entity and httpstatus of the code
	 */

//	@GetMapping(path = "hospitalSubcategoryDisease/{hospitalSubcategoryName}")
//	public ResponseEntity<HospitalSubcategoryDisease> getHospitalSubcategoryDisease(
//			@PathVariable("hospitalSubcategoryName") String hospitalSubcategoryName) {
//		HospitalSubcategoryDisease hsd = null;
//		try {
//			hsd = hospitalSubcategoryDiseaseService
//					.getHospitalSubcategoryDisease(hospitalSubcategoryName);
//		} catch (Exception e) {
//		}
//		return ResponseEntity.status(HttpStatus.OK).body(hsd);
//	}


	/** Api for updating data
	 * passing the param to the service class
	 * @param hospitalCategory
	 * @return httpstatus of the code
	 * @throws EhospitalException
	 */
	@PutMapping(path = "hospitalSubcategoryDiseaseupdate/{hospitalSubcategoryId}")
	public ResponseEntity<HospitalSubcategoryDisease> saveOrUpdateHospitalSubcategoryDisease(
			@RequestBody HospitalSubcategoryDisease hospitalSubcategoryDisease,@PathVariable("hospitalSubcategoryId")int hospitalSubcategoryId) throws EhospitalException {
		HospitalSubcategoryDisease hsd = hospitalSubcategoryDiseaseService
				.updateHospitalSubcategoryDisease(hospitalSubcategoryDisease,hospitalSubcategoryId);
		return ResponseEntity.status(HttpStatus.OK).body(hsd);
	}

	@GetMapping("/getHsc")
	public ResponseEntity<List<HospitalSubcategoryDisease>> getAllHsc() throws EhospitalException
	{
		List<HospitalSubcategoryDisease> ltl = hospitalSubcategoryDiseaseService.getAllHsc();

		return ResponseEntity.status(HttpStatus.OK).body(ltl);
	}


//	@PutMapping("hossubcat/{hospitalSubcategoryId}")
//	public ResponseEntity<HospitalSubcategoryDisease> updateHosSubcat(@PathVariable("hospitalSubcategoryId") int hospitalSubcategoryId,@PathVariable("file") MultipartFile file) throws EhospitalException, IOException, SerialException
//			, SQLException
//	{
//		HospitalSubcategoryDisease ltl = hospitalSubcategoryDiseaseService.updateHosSubcat(file,hospitalSubcategoryId);
//
//		return ResponseEntity.status(HttpStatus.OK).body(ltl);
//	}

	@GetMapping(path = "hsc/{hospitalcategoryid}")
	public ResponseEntity<HospitalSubcategoryDisease> find(
			@PathVariable("hospitalcategoryid") Integer hospitalcategoryid) {
		HospitalSubcategoryDisease hsd = null;
		try {
			hsd = hospitalSubcategoryDiseaseService
					.find(hospitalcategoryid);
		} catch (Exception e) {
		}
		return  ResponseEntity.status(HttpStatus.OK).body(hsd);
	}



	@GetMapping ("/hos/{hospitalcategoryid}")
	public ResponseEntity<HospitalSubcategoryDisease> getHossubcatid(@PathVariable Integer hospitalcategoryid){

		HospitalSubcategoryDisease hossubcat= hospitalSubcategoryDiseaseService.getHossubcatid(hospitalcategoryid);

		if(hossubcat != null){

			return new ResponseEntity<HospitalSubcategoryDisease>(hossubcat, HttpStatus.OK);
		}
		return new ResponseEntity<HospitalSubcategoryDisease>(HttpStatus.BAD_REQUEST);

	}


	@GetMapping("hscat/{hospitalcategoryid}")
	public ResponseEntity < HospitalSubcategoryDisease > getHlScd(
			@PathVariable(value = "hospitalcategoryid") Integer hospitalcategoryid) throws EhospitalException {
		HospitalSubcategoryDisease user = hospitalSubcategoryDiseaseDao.findById(hospitalcategoryid)
				.orElseThrow(() -> new HospitalException("Id not found :: " + hospitalcategoryid));
		return ResponseEntity.ok().body(user);
	}


//	@GetMapping("/getHscatt")
//	public ResponseEntity<List<HospitalSubcategoryDisease>> getAllLabtestlist() throws EhospitalException
//	{
//		List<HospitalSubcategoryDisease> ltl = hospitalSubcategoryDiseaseService.getAllHscatt();
//
//		return ResponseEntity.status(HttpStatus.OK).body(ltl);
//	}


	@GetMapping("hospitalsubcategory/{hospitalCategoryId}")
	public ResponseEntity<List<HospitalSubcategoryDisease>> getHscatByHsatId(@PathVariable("hospitalCategoryId") Integer hospitalCategoryId)
			throws EhospitalException {


		List<HospitalSubcategoryDisease> dd1 = hospitalSubcategoryDiseaseService.getHscatByHsatId(hospitalCategoryId);
		return ResponseEntity.status(HttpStatus.OK).body(dd1);


	}


	@GetMapping ("/Hsactt/{hospitalSubcategoryId}")
	public ResponseEntity<HospitalSubcategoryDisease> getHscatForPatient(@PathVariable Integer hospitalSubcategoryId){

		HospitalSubcategoryDisease Hscat= hospitalSubcategoryDiseaseService.getHscatForPatient(hospitalSubcategoryId);

		if(Hscat != null){

			return new ResponseEntity<HospitalSubcategoryDisease>(Hscat, HttpStatus.OK);
		}
		return new ResponseEntity<HospitalSubcategoryDisease>(HttpStatus.BAD_REQUEST);

	}


	@GetMapping("hossubca/{hospitalCategoryName}")
	public ResponseEntity<List<HospitalSubcategoryDisease>> getHscatByHsatName(@PathVariable("hospitalSubcategoryName") HospitalCategory hospitalCategoryName)
			throws EhospitalException {


		List<HospitalSubcategoryDisease> Hn = hospitalSubcategoryDiseaseService.getHscatByHsatName(hospitalCategoryName);
		return ResponseEntity.status(HttpStatus.OK).body(Hn);


	}


	@GetMapping("hossubcatt/{hospitalCategoryId}")
	public ResponseEntity<List<HospitalSubcategoryDisease>> getHscatByHsatId(@PathVariable("hospitalCategoryId") HospitalCategory hospitalCategoryId)
			throws EhospitalException {


		List<HospitalSubcategoryDisease> Hn = hospitalSubcategoryDiseaseService.getHscatByHsatId(hospitalCategoryId);
		return ResponseEntity.status(HttpStatus.OK).body(Hn);


	}

//	@PostMapping(path = "hospitalSubcategoryDisease/{hospitalCategoryId}")
//	public ResponseEntity<HospitalSubcategoryDisease> addHospitalSubcategoryDisease(
//			@RequestBody HospitalSubcategoryDisease hospitalSubcategoryDisease,@PathVariable("hospitalCategoryId") Integer hospitalCategoryId)
//	{
//		HospitalSubcategoryDisease hsd = hospitalSubcategoryDiseaseService
//				.savehossubcat(hospitalSubcategoryDisease,hospitalCategoryId);
//
//		return ResponseEntity.status(HttpStatus.OK).body(hsd);
//	}


}
