package com.hollongtree.ehospital.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.StateId;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.StateIdService;

@RestController
public class StateIdController {
	
	@Autowired
	StateIdService stateIdService;
	
	/**
	 * API for adding the data to the database
	 * passing the values to the service class
	 * @param stateId
	 * @return
	 */
	@PostMapping(path= "stateId")
	public ResponseEntity<StateId> addEntryStateId(@RequestBody StateId stateId) 
	{
		StateId sd=stateIdService.addStateId(stateId);
		if(sd==null)
		{
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(sd);
	
		}
		return ResponseEntity.status(HttpStatus.OK).body(sd);
		
	}
	/**
	 * API for fetching the data from the database using statename
	 * passing the values to the database
	 * @param stateName
	 * @return
	 * @throws EhospitalException
	 */
	@GetMapping(path="stateId/{stateName}")
	public ResponseEntity<List<StateId>> getStateId(@PathVariable("stateName") String stateName) throws EhospitalException  
	{
		List<StateId> sd = stateIdService.getstateId(stateName);
		
		return ResponseEntity.status(HttpStatus.OK).body(sd);
	}
	/**
	 * API for Updating the records
	 * @param stateId
	 * @return
	 * @throws EhospitalException 
	 */
	
	@PutMapping(path = "stateIdupdate/{stateId}")
	public ResponseEntity<StateId> saveOrUpdateStateId(@RequestBody StateId stateId,@PathVariable("stateId")int stateIdd) throws EhospitalException
	{
		StateId sd=stateIdService.updateStateId(stateId,stateIdd);
		return ResponseEntity.status(HttpStatus.OK).body(sd);
		
	}

}
