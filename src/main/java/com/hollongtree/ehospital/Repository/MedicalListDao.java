package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.MedicalList;

@Repository
public interface MedicalListDao extends JpaRepository<MedicalList, Integer>{

	MedicalList findByMedicalName(String medicalName);

}
