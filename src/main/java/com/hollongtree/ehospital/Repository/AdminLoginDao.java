package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.AdminLogin;

@Repository
public interface AdminLoginDao extends JpaRepository<AdminLogin, Integer>{

	AdminLogin findByemail(String email);


}
