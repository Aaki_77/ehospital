package com.hollongtree.ehospital.Repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.CityList;



@Repository
public interface CityListDao extends JpaRepository<CityList, Integer>{

	CityList findBycityName(String cityName);

}
