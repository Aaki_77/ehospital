package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.Registration;

@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Integer> {

}
