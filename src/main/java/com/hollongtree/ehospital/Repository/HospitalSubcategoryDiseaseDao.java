package com.hollongtree.ehospital.Repository;

import com.hollongtree.ehospital.entity.HospitalCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.HospitalSubcategoryDisease;

import java.util.List;

@Repository
public interface HospitalSubcategoryDiseaseDao extends JpaRepository<HospitalSubcategoryDisease, Integer>{

    @Query(value = "select * from hospital_subcategory_disease  where hospital_category_name =?1",nativeQuery = true)
    List<HospitalSubcategoryDisease> findByhospitalSubcategoryName(String hospitalCategoryName);

    @Query(value = "select * from hospital_subcategory_disease\n" +
            "where hospital_category_id=?1",nativeQuery = true)
    List <HospitalSubcategoryDisease> fetchHsubcatBysHcatId(int hospitalCategoryId);


    @Query(value = "select * from hospital_subcategory_disease\n" +
            "where hospital_subcategory_id=?1",nativeQuery = true)
    List <HospitalSubcategoryDisease> fetchHsactBysubcatId(int hospitalSubcategoryId);

    @Query(value = "select * from hospital_category where hospital_category_name =?yoga",nativeQuery = true)
    List <HospitalSubcategoryDisease> fetchHsubcatByHscatName(HospitalCategory hospitalCategoryName);


    @Query(value = "Insert into doctorDetails where hospital_category_id =?1",nativeQuery = true)
    List <HospitalSubcategoryDisease> fetchHsubcatByHscatId(HospitalCategory hospitalCategoryId);


}




