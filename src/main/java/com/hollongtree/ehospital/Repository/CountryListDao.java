package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.CountryList;

@Repository
public interface CountryListDao extends JpaRepository<CountryList, Integer>{

	CountryList findByCountryName(String countryName);

}
