package com.hollongtree.ehospital.Repository;

import com.hollongtree.ehospital.entity.LabTestList;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LabtestListRepository extends JpaRepository<LabTestList,Integer> {

	//LabTestList getByHospitalId(int hospitalId);

	List<LabTestList> findByHospitalId(int hospitalId);

	@Modifying
	@Transactional
	@Query(value = "UPDATE lab_test_list SET testImage = ?2 WHERE lab_test_list_id=?1",nativeQuery = true)
	List<LabTestList> updateltl(int labTestListId, LabTestList ltl);

	List<LabTestList> findByLabTestListId(int labTestListId);

//	List<LabTestList> findByLabTestListId(int labTestListId);
	
   

//	@Query(value = "select * from lab_test_list as lbt " + 
//			"join hospital as hs on hs.hospital_id=lbt.hospitalId where hospital_id=?1",nativeQuery = true)
	//LabTestList getByhospiatlId(int hospitalId);
}
