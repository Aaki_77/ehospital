package com.hollongtree.ehospital.Repository;

import com.hollongtree.ehospital.entity.PrescriptionPage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PageRepository extends JpaRepository<PrescriptionPage,Integer> {

	@Query(value = "select * from page as a join Prescription as b on a.page_id=b.prescription_id join "
			+ "patient as c on b.prescription_id=c.patient_id where c.patient_id=?1", nativeQuery = true)

	PrescriptionPage fetchPatient(String patientid);
}
