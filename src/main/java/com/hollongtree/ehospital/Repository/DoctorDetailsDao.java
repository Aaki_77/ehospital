package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.relational.core.sql.In;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.DoctorDetails;

import java.util.List;

@Repository
public interface DoctorDetailsDao extends JpaRepository<DoctorDetails, Integer> {

	@Query(value = "select * from doctor_details where doctor_name=?1",nativeQuery = true)
	DoctorDetails findByDoctorname(String doctorName);

	@Query(value = "select * from doctor_details where patient_patient_id=?1",nativeQuery = true)
	DoctorDetails fetchDoctorByPatientid(String patientid);

	@Query(value = "select * from doctor_details\n" +
			"where hospital_subcategory_id=?1",nativeQuery = true)
	List <DoctorDetails> fetchDoctorDetailsBysubcatId(int hospitalSubcategoryId);


	@Query(value = "select * from doctor_details\n" +
			"where hospital_subcategory_id=?2",nativeQuery = true)
	List <DoctorDetails> fetchDoctorDetailsBysubcatId2(int hospitalSubcategoryId);


	@Query(value = "select * from doctor_details\n" +
			"where hospital_subcategory_id=?3",nativeQuery = true)
	List <DoctorDetails> fetchDoctorDetailsBysubcatId3(int hospitalSubcategoryId);

	@Query(value = "select * from doctor_details\n" +
			"where doctor_details_id <=3;",nativeQuery = true)
	DoctorDetails saveall();


	@Query(value = "select * from doctor_details where doctor_details_id=?1",nativeQuery = true)
	List<DoctorDetails> findByDoctorId(Integer doctorDetailsId);


	@Query(value = "select * from doctor_details\n" +
			"where hospital_subcategory_id=?11",nativeQuery = true)
	List <DoctorDetails> fetchDoctorDetailsBysubcatId11(int hospitalSubcategoryId);

}
