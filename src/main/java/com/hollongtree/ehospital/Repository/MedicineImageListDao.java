package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.MedicineImageList;

@Repository
public interface MedicineImageListDao extends JpaRepository<MedicineImageList, Integer>{

}
