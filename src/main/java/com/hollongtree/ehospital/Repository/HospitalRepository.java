package com.hollongtree.ehospital.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.Hospital;

@Repository
public interface HospitalRepository extends JpaRepository<Hospital, Integer>
{

	Hospital findById(int id);

	@Query(value = "select * from hospital where hospital_name=?1",nativeQuery = true)
	Hospital findByHospitalname(String hospitalName);

}
