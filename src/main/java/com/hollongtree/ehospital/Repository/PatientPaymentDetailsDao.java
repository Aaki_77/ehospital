package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.PatientPaymentDetails;

@Repository
public interface PatientPaymentDetailsDao extends JpaRepository<PatientPaymentDetails, Integer>
{

	PatientPaymentDetails findByPatientsName(String patientsName);

	PatientPaymentDetails findByPatientsId(int patientsId);

}
