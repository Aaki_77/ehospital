package com.hollongtree.ehospital.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.MedicineList;

@Repository
public interface MedicineListDao extends JpaRepository<MedicineList, Integer>{

	MedicineList findByMedicineName(String medicineName);

	@Query(value = "select * from medicine_list where medicine_id=?1",nativeQuery = true)
	List<MedicineList> findByMedicineId(int medicineId);

}
