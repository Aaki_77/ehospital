package com.hollongtree.ehospital;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.RestController;



@SpringBootApplication
@RestController
public class EhospitalApplication extends SpringBootServletInitializer {

	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {

		return application.sources(EhospitalApplication.class);
	}
	public static void main(String[] args) {
		SpringApplication.run(EhospitalApplication.class, args);

	}
		
	}



